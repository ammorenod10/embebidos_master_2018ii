/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/*
 * This file has been automatically generated using ChibiStudio board
 * generator plugin. Do not edit manually.
 */

#ifndef BOARD_H
#define BOARD_H

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/*
 * Setup for STMicroelectronics STM32 Nucleo144-F446ZE board.
 */

/*
 * Board identifier.
 */
#define BOARD_COLUMBA
#define BOARD_NAME                  "COLUMBA"

/*
 * Board oscillators-related settings.
 * NOTE: LSE not fitted.
 */
#if !defined(STM32_LSECLK)
#define STM32_LSECLK                32768U
#endif

#if !defined(STM32_HSECLK)
#define STM32_HSECLK                16000000U
#endif

#define STM32_HSE_BYPASS

/*
 * Board voltages.
 * Required for performance limits calculation.
 */
#define STM32_VDD                   300U

/*
 * MCU type as defined in the ST header.
 */
#define STM32F446xx

/*
 * IO pins assignments.
 */
#define GPIOA_PIN0                  0U
#define GPIOA_PIN1                  1U
#define GPIOA_SSR1                  2U
#define GPIOA_MODE2                 3U
#define GPIOA_MODE1                 4U
#define GPIOA_MODE0                 5U
#define GPIOA_STEP                  6U
#define GPIOA_ENBL                  7U
#define GPIOA_LED1                  8U
#define GPIOA_PIN9                  9U
#define GPIOA_PIN10                 10U
#define GPIOA_PIN11                 11U
#define GPIOA_PIN12                 12U
#define GPIOA_PIN13                 13U
#define GPIOA_PIN14                 14U
#define GPIOA_PIN15                 15U

#define GPIOB_RESET                 0U
#define GPIOB_PIN1                  1U
#define GPIOB_PIN2                  2U
#define GPIOB_PIN3                  3U
#define GPIOB_PIN4                  4U
#define GPIOB_PIN5                  5U
#define GPIOB_PIN6                  6U
#define GPIOB_FMC_NL                7U
#define GPIOB_PIN8                  8U
#define GPIOB_PIN9                  9U
#define GPIOB_PIN10                 10U
#define GPIOB_PIN11                 11U
#define GPIOB_LED4                  12U
#define GPIOB_PIN13                 13U
#define GPIOB_PIN14                 14U
#define GPIOB_LED3                  15U

#define GPIOC_PIN0                  0U
#define GPIOC_PIN1                  1U
#define GPIOC_ADC12_IN12            2U
#define GPIOC_ADC12_IN13            3U
#define GPIOC_DIR                   4U
#define GPIOC_SLEEP                 5U
#define GPIOC_PIN6                  6U
#define GPIOC_PIN7                  7U
#define GPIOC_PIN8                  8U
#define GPIOC_PIN9                  9U
#define GPIOC_STM_TX                10U
#define GPIOC_STM_RX                11U
#define GPIOC_PIN12                 12U
#define GPIOC_PIN13                 13U
#define GPIOC_OSC32_IN              14U
#define GPIOC_OSC32_OUT             15U

#define GPIOD_FMC_D2                0U
#define GPIOD_FMC_D3                1U
#define GPIOD_PIN2                  2U
#define GPIOD_FMC_CLK               3U
#define GPIOD_FMC_NOE               4U
#define GPIOD_FMC_NWE               5U
#define GPIOD_FMC_NWAIT             6U
#define GPIOD_PIN7                  7U
#define GPIOD_FMC_D13               8U
#define GPIOD_FMC_D14               9U
#define GPIOD_FMC_D15               10U
#define GPIOD_FMC_A16               11U
#define GPIOD_FMC_A17               12U
#define GPIOD_FMC_A18               13U
#define GPIOD_FMC_D0                14U
#define GPIOD_FMC_D1                15U

#define GPIOE_FMC_NBL0              0U
#define GPIOE_FMC_NBL1              1U
#define GPIOE_FMC_A23               2U
#define GPIOE_FMC_A19               3U
#define GPIOE_FMC_A20               4U
#define GPIOE_FMC_A21               5U
#define GPIOE_FMC_A22               6U
#define GPIOE_FMC_D4                7U
#define GPIOE_FMC_D5                8U
#define GPIOE_FMC_D6                9U
#define GPIOE_FMC_D7                10U
#define GPIOE_FMC_D8                11U
#define GPIOE_FMC_D9                12U
#define GPIOE_FMC_D10               13U
#define GPIOE_FMC_D11               14U
#define GPIOE_FMC_D12               15U

#define GPIOF_I2C2_SDA              0U
#define GPIOF_I2C2_SCL              1U
#define GPIOF_PIN2                  2U
#define GPIOF_PIN3                  3U
#define GPIOF_PIN4                  4U
#define GPIOF_ADC3_IN15             5U
#define GPIOF_DIV_CLK               6U
#define GPIOF_ADC3_IN4              7U
#define GPIOF_ADC3_IN5              8U
#define GPIOF_ADC3_IN6              9U
#define GPIOF_PIN10                 10U
#define GPIOF_PIN11                 11U
#define GPIOF_PIN12                 12U
#define GPIOF_PIN13                 13U
#define GPIOF_PIN14                 14U
#define GPIOF_PIN15                 15U

#define GPIOG_PIN0                  0U
#define GPIOG_PIN1                  1U
#define GPIOG_PIN2                  2U
#define GPIOG_PIN3                  3U
#define GPIOG_LED2                  4U
#define GPIOG_PIN5                  5U
#define GPIOG_PIN6                  6U
#define GPIOG_PIN7                  7U
#define GPIOG_PIN8                  8U
#define GPIOG_FMC_NE2               9U
#define GPIOG_PIN10                 10U
#define GPIOG_PIN11                 11U
#define GPIOG_PIN12                 12U
#define GPIOG_FMC_A24               13U
#define GPIOG_FMC_A25               14U
#define GPIOG_PIN15                 15U

#define GPIOH_OSC_IN                0U
#define GPIOH_OSC_OUT               1U
#define GPIOH_PIN2                  2U
#define GPIOH_PIN3                  3U
#define GPIOH_PIN4                  4U
#define GPIOH_PIN5                  5U
#define GPIOH_PIN6                  6U
#define GPIOH_PIN7                  7U
#define GPIOH_PIN8                  8U
#define GPIOH_PIN9                  9U
#define GPIOH_PIN10                 10U
#define GPIOH_PIN11                 11U
#define GPIOH_PIN12                 12U
#define GPIOH_PIN13                 13U
#define GPIOH_PIN14                 14U
#define GPIOH_PIN15                 15U

#define GPIOI_PIN0                  0U
#define GPIOI_PIN1                  1U
#define GPIOI_PIN2                  2U
#define GPIOI_PIN3                  3U
#define GPIOI_PIN4                  4U
#define GPIOI_PIN5                  5U
#define GPIOI_PIN6                  6U
#define GPIOI_PIN7                  7U
#define GPIOI_PIN8                  8U
#define GPIOI_PIN9                  9U
#define GPIOI_PIN10                 10U
#define GPIOI_PIN11                 11U
#define GPIOI_PIN12                 12U
#define GPIOI_PIN13                 13U
#define GPIOI_PIN14                 14U
#define GPIOI_PIN15                 15U

#define GPIOJ_PIN0                  0U
#define GPIOJ_PIN1                  1U
#define GPIOJ_PIN2                  2U
#define GPIOJ_PIN3                  3U
#define GPIOJ_PIN4                  4U
#define GPIOJ_PIN5                  5U
#define GPIOJ_PIN6                  6U
#define GPIOJ_PIN7                  7U
#define GPIOJ_PIN8                  8U
#define GPIOJ_PIN9                  9U
#define GPIOJ_PIN10                 10U
#define GPIOJ_PIN11                 11U
#define GPIOJ_PIN12                 12U
#define GPIOJ_PIN13                 13U
#define GPIOJ_PIN14                 14U
#define GPIOJ_PIN15                 15U

#define GPIOK_PIN0                  0U
#define GPIOK_PIN1                  1U
#define GPIOK_PIN2                  2U
#define GPIOK_PIN3                  3U
#define GPIOK_PIN4                  4U
#define GPIOK_PIN5                  5U
#define GPIOK_PIN6                  6U
#define GPIOK_PIN7                  7U
#define GPIOK_PIN8                  8U
#define GPIOK_PIN9                  9U
#define GPIOK_PIN10                 10U
#define GPIOK_PIN11                 11U
#define GPIOK_PIN12                 12U
#define GPIOK_PIN13                 13U
#define GPIOK_PIN14                 14U
#define GPIOK_PIN15                 15U

/*
 * IO lines assignments.
 */
#define LINE_SSR1                  PAL_LINE(GPIOA, 2U)
#define LINE_MODE2                 PAL_LINE(GPIOA, 3U)
#define LINE_MODE1                 PAL_LINE(GPIOA, 4U)
#define LINE_MODE0                 PAL_LINE(GPIOA, 5U)
#define LINE_STEP                  PAL_LINE(GPIOA, 6U)
#define LINE_ENBL                  PAL_LINE(GPIOA, 7U)
#define LINE_LED1                  PAL_LINE(GPIOA, 8U)
#define LINE_RESET                 PAL_LINE(GPIOB, 0U)
#define LINE_FMC_NL                PAL_LINE(GPIOB, 7U)
#define LINE_LED4                  PAL_LINE(GPIOB, 12U)
#define LINE_LED3                  PAL_LINE(GPIOB, 15U)
#define LINE_ADC12_IN12            PAL_LINE(GPIOC, 2U)
#define LINE_ADC12_IN13            PAL_LINE(GPIOC, 3U)
#define LINE_DIR                   PAL_LINE(GPIOC, 4U)
#define LINE_SLEEP                 PAL_LINE(GPIOC, 5U)
#define LINE_STM_TX                PAL_LINE(GPIOC, 10U)
#define LINE_STM_RX                PAL_LINE(GPIOC, 11U)
#define LINE_OSC32_IN              PAL_LINE(GPIOC, 14U)
#define LINE_OSC32_OUT             PAL_LINE(GPIOC, 15U)
#define LINE_FMC_D2                PAL_LINE(GPIOD, 0U)
#define LINE_FMC_D3                PAL_LINE(GPIOD, 1U)
#define LINE_FMC_CLK               PAL_LINE(GPIOD, 3U)
#define LINE_FMC_NOE               PAL_LINE(GPIOD, 4U)
#define LINE_FMC_NWE               PAL_LINE(GPIOD, 5U)
#define LINE_FMC_NWAIT             PAL_LINE(GPIOD, 6U)
#define LINE_FMC_D13               PAL_LINE(GPIOD, 8U)
#define LINE_FMC_D14               PAL_LINE(GPIOD, 9U)
#define LINE_FMC_D15               PAL_LINE(GPIOD, 10U)
#define LINE_FMC_A16               PAL_LINE(GPIOD, 11U)
#define LINE_FMC_A17               PAL_LINE(GPIOD, 12U)
#define LINE_FMC_A18               PAL_LINE(GPIOD, 13U)
#define LINE_FMC_D0                PAL_LINE(GPIOD, 14U)
#define LINE_FMC_D1                PAL_LINE(GPIOD, 15U)
#define LINE_FMC_NBL0              PAL_LINE(GPIOE, 0U)
#define LINE_FMC_NBL1              PAL_LINE(GPIOE, 1U)
#define LINE_FMC_A23               PAL_LINE(GPIOE, 2U)
#define LINE_FMC_A19               PAL_LINE(GPIOE, 3U)
#define LINE_FMC_A20               PAL_LINE(GPIOE, 4U)
#define LINE_FMC_A21               PAL_LINE(GPIOE, 5U)
#define LINE_FMC_A22               PAL_LINE(GPIOE, 6U)
#define LINE_FMC_D4                PAL_LINE(GPIOE, 7U)
#define LINE_FMC_D5                PAL_LINE(GPIOE, 8U)
#define LINE_FMC_D6                PAL_LINE(GPIOE, 9U)
#define LINE_FMC_D7                PAL_LINE(GPIOE, 10U)
#define LINE_FMC_D8                PAL_LINE(GPIOE, 11U)
#define LINE_FMC_D9                PAL_LINE(GPIOE, 12U)
#define LINE_FMC_D10               PAL_LINE(GPIOE, 13U)
#define LINE_FMC_D11               PAL_LINE(GPIOE, 14U)
#define LINE_FMC_D12               PAL_LINE(GPIOE, 15U)
#define LINE_I2C2_SDA              PAL_LINE(GPIOF, 0U)
#define LINE_I2C2_SCL              PAL_LINE(GPIOF, 1U)
#define LINE_ADC3_IN15             PAL_LINE(GPIOF, 5U)
#define LINE_DIV_CLK               PAL_LINE(GPIOF, 6U)
#define LINE_ADC3_IN4              PAL_LINE(GPIOF, 7U)
#define LINE_ADC3_IN5              PAL_LINE(GPIOF, 8U)
#define LINE_ADC3_IN6              PAL_LINE(GPIOF, 9U)
#define LINE_LED2                  PAL_LINE(GPIOG, 4U)
#define LINE_FMC_NE2               PAL_LINE(GPIOG, 9U)
#define LINE_FMC_A24               PAL_LINE(GPIOG, 13U)
#define LINE_FMC_A25               PAL_LINE(GPIOG, 14U)
#define LINE_OSC_IN                PAL_LINE(GPIOH, 0U)
#define LINE_OSC_OUT               PAL_LINE(GPIOH, 1U)

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*
 * I/O ports initial setup, this configuration is established soon after reset
 * in the initialization code.
 * Please refer to the STM32 Reference Manual for details.
 */
#define PIN_MODE_INPUT(n)           (0U << ((n) * 2U))
#define PIN_MODE_OUTPUT(n)          (1U << ((n) * 2U))
#define PIN_MODE_ALTERNATE(n)       (2U << ((n) * 2U))
#define PIN_MODE_ANALOG(n)          (3U << ((n) * 2U))
#define PIN_ODR_LOW(n)              (0U << (n))
#define PIN_ODR_HIGH(n)             (1U << (n))
#define PIN_OTYPE_PUSHPULL(n)       (0U << (n))
#define PIN_OTYPE_OPENDRAIN(n)      (1U << (n))
#define PIN_OSPEED_VERYLOW(n)       (0U << ((n) * 2U))
#define PIN_OSPEED_LOW(n)           (1U << ((n) * 2U))
#define PIN_OSPEED_MEDIUM(n)        (2U << ((n) * 2U))
#define PIN_OSPEED_HIGH(n)          (3U << ((n) * 2U))
#define PIN_PUPDR_FLOATING(n)       (0U << ((n) * 2U))
#define PIN_PUPDR_PULLUP(n)         (1U << ((n) * 2U))
#define PIN_PUPDR_PULLDOWN(n)       (2U << ((n) * 2U))
#define PIN_AFIO_AF(n, v)           ((v) << (((n) % 8U) * 4U))

/*
 * GPIOA setup:
 *
 * PA0  - PIN0                      (input pullup).
 * PA1  - PIN1                      (input pullup).
 * PA2  - SSR1                      (output pushpull maximum).
 * PA3  - MODE2                     (output pushpull maximum).
 * PA4  - MODE1                     (output pushpull maximum).
 * PA5  - MODE0                     (output pushpull maximum).
 * PA6  - STEP                      (output pushpull maximum).
 * PA7  - ENBL                      (output pushpull maximum).
 * PA8  - LED1                      (output pushpull maximum).
 * PA9  - PIN9                      (input pullup).
 * PA10 - PIN10                     (input pullup).
 * PA11 - PIN11                     (input pullup).
 * PA12 - PIN12                     (input pullup).
 * PA13 - PIN13                     (input pullup).
 * PA14 - PIN14                     (input pullup).
 * PA15 - PIN15                     (input pullup).
 */
#define VAL_GPIOA_MODER             (PIN_MODE_INPUT(GPIOA_PIN0) |           \
                                     PIN_MODE_INPUT(GPIOA_PIN1) |           \
                                     PIN_MODE_OUTPUT(GPIOA_SSR1) |          \
                                     PIN_MODE_OUTPUT(GPIOA_MODE2) |         \
                                     PIN_MODE_OUTPUT(GPIOA_MODE1) |         \
                                     PIN_MODE_OUTPUT(GPIOA_MODE0) |         \
                                     PIN_MODE_OUTPUT(GPIOA_STEP) |          \
                                     PIN_MODE_OUTPUT(GPIOA_ENBL) |          \
                                     PIN_MODE_OUTPUT(GPIOA_LED1) |          \
                                     PIN_MODE_INPUT(GPIOA_PIN9) |           \
                                     PIN_MODE_INPUT(GPIOA_PIN10) |          \
                                     PIN_MODE_INPUT(GPIOA_PIN11) |          \
                                     PIN_MODE_INPUT(GPIOA_PIN12) |          \
                                     PIN_MODE_INPUT(GPIOA_PIN13) |          \
                                     PIN_MODE_INPUT(GPIOA_PIN14) |          \
                                     PIN_MODE_INPUT(GPIOA_PIN15))
#define VAL_GPIOA_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOA_PIN0) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOA_PIN1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOA_SSR1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOA_MODE2) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOA_MODE1) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOA_MODE0) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOA_STEP) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOA_ENBL) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOA_LED1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOA_PIN9) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOA_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOA_PIN11) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOA_PIN12) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOA_PIN13) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOA_PIN14) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOA_PIN15))
#define VAL_GPIOA_OSPEEDR           (PIN_OSPEED_VERYLOW(GPIOA_PIN0) |       \
                                     PIN_OSPEED_VERYLOW(GPIOA_PIN1) |       \
                                     PIN_OSPEED_HIGH(GPIOA_SSR1) |          \
                                     PIN_OSPEED_HIGH(GPIOA_MODE2) |         \
                                     PIN_OSPEED_HIGH(GPIOA_MODE1) |         \
                                     PIN_OSPEED_HIGH(GPIOA_MODE0) |         \
                                     PIN_OSPEED_HIGH(GPIOA_STEP) |          \
                                     PIN_OSPEED_HIGH(GPIOA_ENBL) |          \
                                     PIN_OSPEED_HIGH(GPIOA_LED1) |          \
                                     PIN_OSPEED_HIGH(GPIOA_PIN9) |          \
                                     PIN_OSPEED_VERYLOW(GPIOA_PIN10) |      \
                                     PIN_OSPEED_VERYLOW(GPIOA_PIN11) |      \
                                     PIN_OSPEED_VERYLOW(GPIOA_PIN12) |      \
                                     PIN_OSPEED_VERYLOW(GPIOA_PIN13) |      \
                                     PIN_OSPEED_VERYLOW(GPIOA_PIN14) |      \
                                     PIN_OSPEED_VERYLOW(GPIOA_PIN15))
#define VAL_GPIOA_PUPDR             (PIN_PUPDR_PULLUP(GPIOA_PIN0) |         \
                                     PIN_PUPDR_PULLUP(GPIOA_PIN1) |         \
                                     PIN_PUPDR_FLOATING(GPIOA_SSR1) |       \
                                     PIN_PUPDR_FLOATING(GPIOA_MODE2) |      \
                                     PIN_PUPDR_FLOATING(GPIOA_MODE1) |      \
                                     PIN_PUPDR_FLOATING(GPIOA_MODE0) |      \
                                     PIN_PUPDR_FLOATING(GPIOA_STEP) |       \
                                     PIN_PUPDR_FLOATING(GPIOA_ENBL) |       \
                                     PIN_PUPDR_FLOATING(GPIOA_LED1) |       \
                                     PIN_PUPDR_PULLUP(GPIOA_PIN9) |         \
                                     PIN_PUPDR_PULLUP(GPIOA_PIN10) |        \
                                     PIN_PUPDR_PULLUP(GPIOA_PIN11) |        \
                                     PIN_PUPDR_PULLUP(GPIOA_PIN12) |        \
                                     PIN_PUPDR_PULLUP(GPIOA_PIN13) |        \
                                     PIN_PUPDR_PULLUP(GPIOA_PIN14) |        \
                                     PIN_PUPDR_PULLUP(GPIOA_PIN15))
#define VAL_GPIOA_ODR               (PIN_ODR_HIGH(GPIOA_PIN0) |             \
                                     PIN_ODR_HIGH(GPIOA_PIN1) |             \
                                     PIN_ODR_LOW(GPIOA_SSR1) |              \
                                     PIN_ODR_LOW(GPIOA_MODE2) |             \
                                     PIN_ODR_LOW(GPIOA_MODE1) |             \
                                     PIN_ODR_LOW(GPIOA_MODE0) |             \
                                     PIN_ODR_LOW(GPIOA_STEP) |              \
                                     PIN_ODR_LOW(GPIOA_ENBL) |              \
                                     PIN_ODR_LOW(GPIOA_LED1) |              \
                                     PIN_ODR_HIGH(GPIOA_PIN9) |             \
                                     PIN_ODR_HIGH(GPIOA_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOA_PIN11) |            \
                                     PIN_ODR_HIGH(GPIOA_PIN12) |            \
                                     PIN_ODR_HIGH(GPIOA_PIN13) |            \
                                     PIN_ODR_HIGH(GPIOA_PIN14) |            \
                                     PIN_ODR_HIGH(GPIOA_PIN15))
#define VAL_GPIOA_AFRL              (PIN_AFIO_AF(GPIOA_PIN0, 0U) |          \
                                     PIN_AFIO_AF(GPIOA_PIN1, 0U) |          \
                                     PIN_AFIO_AF(GPIOA_SSR1, 0U) |          \
                                     PIN_AFIO_AF(GPIOA_MODE2, 0U) |         \
                                     PIN_AFIO_AF(GPIOA_MODE1, 0U) |         \
                                     PIN_AFIO_AF(GPIOA_MODE0, 0U) |         \
                                     PIN_AFIO_AF(GPIOA_STEP, 0U) |          \
                                     PIN_AFIO_AF(GPIOA_ENBL, 0U))
#define VAL_GPIOA_AFRH              (PIN_AFIO_AF(GPIOA_LED1, 0U) |          \
                                     PIN_AFIO_AF(GPIOA_PIN9, 0U) |          \
                                     PIN_AFIO_AF(GPIOA_PIN10, 0U) |         \
                                     PIN_AFIO_AF(GPIOA_PIN11, 0U) |         \
                                     PIN_AFIO_AF(GPIOA_PIN12, 0U) |         \
                                     PIN_AFIO_AF(GPIOA_PIN13, 0U) |         \
                                     PIN_AFIO_AF(GPIOA_PIN14, 0U) |         \
                                     PIN_AFIO_AF(GPIOA_PIN15, 0U))

/*
 * GPIOB setup:
 *
 * PB0  - RESET                     (output pushpull maximum).
 * PB1  - PIN1                      (input pullup).
 * PB2  - PIN2                      (input pullup).
 * PB3  - PIN3                      (input pullup).
 * PB4  - PIN4                      (input pullup).
 * PB5  - PIN5                      (input pullup).
 * PB6  - PIN6                      (input pullup).
 * PB7  - FMC_NL                    (alternate 10).
 * PB8  - PIN8                      (input pullup).
 * PB9  - PIN9                      (input pullup).
 * PB10 - PIN10                     (input pullup).
 * PB11 - PIN13                     (input pullup).
 * PB12 - LED4                      (output pushpull maximum).
 * PB13 - PIN13                     (input pullup).
 * PB14 - PIN14                     (output pushpull maximum).
 * PB15 - LED3                      (input pullup).
 */
#define VAL_GPIOB_MODER             (PIN_MODE_OUTPUT(GPIOB_RESET) |         \
                                     PIN_MODE_INPUT(GPIOB_PIN1) |           \
                                     PIN_MODE_INPUT(GPIOB_PIN2) |           \
                                     PIN_MODE_INPUT(GPIOB_PIN3) |           \
                                     PIN_MODE_INPUT(GPIOB_PIN4) |           \
                                     PIN_MODE_INPUT(GPIOB_PIN5) |           \
                                     PIN_MODE_INPUT(GPIOB_PIN6) |           \
                                     PIN_MODE_OUTPUT(GPIOB_FMC_NL) |        \
                                     PIN_MODE_INPUT(GPIOB_PIN8) |           \
                                     PIN_MODE_INPUT(GPIOB_PIN9) |           \
                                     PIN_MODE_INPUT(GPIOB_PIN10) |          \
                                     PIN_MODE_INPUT(GPIOB_PIN11) |          \
                                     PIN_MODE_OUTPUT(GPIOB_LED4) |          \
                                     PIN_MODE_INPUT(GPIOB_PIN13) |          \
                                     PIN_MODE_INPUT(GPIOB_PIN14) |          \
                                     PIN_MODE_OUTPUT(GPIOB_LED3))
#define VAL_GPIOB_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOB_RESET) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN4) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN5) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_FMC_NL) |     \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN8) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN9) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN11) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOB_LED4) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN13) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN14) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOB_LED3))
#define VAL_GPIOB_OSPEEDR           (PIN_OSPEED_HIGH(GPIOB_RESET) |         \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN1) |       \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN2) |       \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN3) |       \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN4) |       \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN5) |       \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN6) |       \
                                     PIN_OSPEED_HIGH(GPIOB_FMC_NL) |        \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN8) |       \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN9) |       \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN10) |      \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN11) |      \
                                     PIN_OSPEED_HIGH(GPIOB_LED4) |          \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN13) |      \
                                     PIN_OSPEED_VERYLOW(GPIOB_PIN14) |      \
                                     PIN_OSPEED_HIGH(GPIOB_LED3))
#define VAL_GPIOB_PUPDR             (PIN_PUPDR_FLOATING(GPIOB_RESET) |      \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN1) |         \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN2) |         \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN3) |         \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN4) |         \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN5) |         \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN6) |         \
                                     PIN_PUPDR_FLOATING(GPIOB_FMC_NL) |     \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN8) |         \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN9) |         \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN10) |        \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN11) |        \
                                     PIN_PUPDR_FLOATING(GPIOB_LED4) |       \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN13) |        \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN14) |        \
                                     PIN_PUPDR_FLOATING(GPIOB_LED3))
#define VAL_GPIOB_ODR               (PIN_ODR_LOW(GPIOB_RESET) |             \
                                     PIN_ODR_HIGH(GPIOB_PIN1) |             \
                                     PIN_ODR_HIGH(GPIOB_PIN2) |             \
                                     PIN_ODR_HIGH(GPIOB_PIN3) |             \
                                     PIN_ODR_HIGH(GPIOB_PIN4) |             \
                                     PIN_ODR_HIGH(GPIOB_PIN5) |             \
                                     PIN_ODR_HIGH(GPIOB_PIN6) |             \
                                     PIN_ODR_LOW(GPIOB_FMC_NL) |            \
                                     PIN_ODR_HIGH(GPIOB_PIN8) |             \
                                     PIN_ODR_HIGH(GPIOB_PIN9) |             \
                                     PIN_ODR_HIGH(GPIOB_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOB_PIN11) |            \
                                     PIN_ODR_LOW(GPIOB_LED4) |              \
                                     PIN_ODR_HIGH(GPIOB_PIN13) |            \
                                     PIN_ODR_HIGH(GPIOB_PIN14) |            \
                                     PIN_ODR_LOW(GPIOB_LED3))
#define VAL_GPIOB_AFRL              (PIN_AFIO_AF(GPIOB_RESET, 0U) |         \
                                     PIN_AFIO_AF(GPIOB_PIN1, 0U) |          \
                                     PIN_AFIO_AF(GPIOB_PIN2, 0U) |          \
                                     PIN_AFIO_AF(GPIOB_PIN3, 0U) |          \
                                     PIN_AFIO_AF(GPIOB_PIN4, 0U) |          \
                                     PIN_AFIO_AF(GPIOB_PIN5, 0U) |          \
                                     PIN_AFIO_AF(GPIOB_PIN6, 0U) |          \
                                     PIN_AFIO_AF(GPIOB_FMC_NL, 10U))
#define VAL_GPIOB_AFRH              (PIN_AFIO_AF(GPIOB_PIN8, 0U) |          \
                                     PIN_AFIO_AF(GPIOB_PIN9, 0U) |          \
                                     PIN_AFIO_AF(GPIOB_PIN10, 0U) |         \
                                     PIN_AFIO_AF(GPIOB_PIN11, 0U) |         \
                                     PIN_AFIO_AF(GPIOB_LED4, 0U) |          \
                                     PIN_AFIO_AF(GPIOB_PIN13, 0U) |         \
                                     PIN_AFIO_AF(GPIOB_PIN14, 0U) |         \
                                     PIN_AFIO_AF(GPIOB_LED3, 0U))

/*
 * GPIOC setup:
 *
 * PC0  - PIN0                      (input pullup).
 * PC1  - PIN1                      (input pullup).
 * PC2  - ADC123_IN12               (input pullup).
 * PC3  - ADC123_IN13               (input pullup).
 * PC4  - DIR                       (output pushpull maximum).
 * PC5  - SLEEP                     (output pushpull maximum).
 * PC6  - PIN6                      (input pullup).
 * PC7  - PIN7                      (input pullup).
 * PC8  - PIN8                      (input pullup).
 * PC9  - PIN9                      (input pullup).
 * PC10 - STM_TX USART3_TX          (alternate 7).
 * PC11 - STM_RX USART3_RX          (alternate 7).
 * PC12 - PIN12                     (input pullup).
 * PC13 - PIN13                     (input floating).
 * PC14 - OSC32_IN                  (input floating).
 * PC15 - OSC32_OUT                 (input floating).
 */
 #define VAL_GPIOC_MODER             (PIN_MODE_INPUT(GPIOC_PIN0) |          \
                                      PIN_MODE_INPUT(GPIOC_PIN1) |          \
                                      PIN_MODE_ANALOG(GPIOC_ADC12_IN12) |   \
                                      PIN_MODE_ANALOG(GPIOC_ADC12_IN13) |   \
                                      PIN_MODE_OUTPUT(GPIOC_DIR) |          \
                                      PIN_MODE_OUTPUT(GPIOC_SLEEP) |        \
                                      PIN_MODE_INPUT(GPIOC_PIN6) |          \
                                      PIN_MODE_INPUT(GPIOC_PIN7) |          \
                                      PIN_MODE_INPUT(GPIOC_PIN8) |          \
                                      PIN_MODE_INPUT(GPIOC_PIN9) |          \
                                      PIN_MODE_ALTERNATE(GPIOC_STM_TX) |    \
                                      PIN_MODE_ALTERNATE(GPIOC_STM_RX) |    \
                                      PIN_MODE_INPUT(GPIOC_PIN12) |         \
                                      PIN_MODE_INPUT(GPIOC_PIN13) |         \
                                      PIN_MODE_INPUT(GPIOC_OSC32_IN) |      \
                                      PIN_MODE_INPUT(GPIOC_OSC32_OUT))
 #define VAL_GPIOC_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOC_PIN0) |      \
                                      PIN_OTYPE_PUSHPULL(GPIOC_PIN1) |      \
                                      PIN_OTYPE_PUSHPULL(GPIOC_ADC12_IN12) | \
                                      PIN_OTYPE_PUSHPULL(GPIOC_ADC12_IN13) | \
                                      PIN_OTYPE_PUSHPULL(GPIOC_DIR) |       \
                                      PIN_OTYPE_PUSHPULL(GPIOC_SLEEP ) |    \
                                      PIN_OTYPE_PUSHPULL(GPIOC_PIN6) |      \
                                      PIN_OTYPE_PUSHPULL(GPIOC_PIN7 ) |     \
                                      PIN_OTYPE_PUSHPULL(GPIOC_PIN8) |      \
                                      PIN_OTYPE_PUSHPULL(GPIOC_PIN9) |      \
                                      PIN_OTYPE_PUSHPULL(GPIOC_STM_TX) |    \
                                      PIN_OTYPE_PUSHPULL(GPIOC_STM_RX ) |   \
                                      PIN_OTYPE_PUSHPULL(GPIOC_PIN12) |     \
                                      PIN_OTYPE_PUSHPULL(GPIOC_PIN13) |     \
                                      PIN_OTYPE_PUSHPULL(GPIOC_OSC32_IN) |  \
                                      PIN_OTYPE_PUSHPULL(GPIOC_OSC32_OUT))
 #define VAL_GPIOC_OSPEEDR           (PIN_OSPEED_VERYLOW(GPIOC_PIN0) |      \
                                      PIN_OSPEED_VERYLOW(GPIOC_PIN1) |      \
                                      PIN_OSPEED_HIGH(GPIOC_ADC12_IN12) |   \
                                      PIN_OSPEED_HIGH(GPIOC_ADC12_IN13) |   \
                                      PIN_OSPEED_HIGH(GPIOC_DIR) |          \
                                      PIN_OSPEED_HIGH(GPIOC_SLEEP) |        \
                                      PIN_OSPEED_VERYLOW(GPIOC_PIN6) |      \
                                      PIN_OSPEED_VERYLOW(GPIOC_PIN7) |      \
                                      PIN_OSPEED_VERYLOW(GPIOC_PIN8) |      \
                                      PIN_OSPEED_VERYLOW(GPIOC_PIN9) |      \
                                      PIN_OSPEED_HIGH(GPIOC_STM_TX) |       \
                                      PIN_OSPEED_HIGH(GPIOC_STM_RX) |       \
                                      PIN_OSPEED_VERYLOW(GPIOC_PIN12) |     \
                                      PIN_OSPEED_VERYLOW(GPIOC_PIN13) |     \
                                      PIN_OSPEED_HIGH(GPIOC_OSC32_IN) |     \
                                      PIN_OSPEED_HIGH(GPIOC_OSC32_OUT))
 #define VAL_GPIOC_PUPDR             (PIN_PUPDR_PULLUP(GPIOC_PIN0) |        \
                                      PIN_PUPDR_PULLUP(GPIOC_PIN1) |        \
                                      PIN_PUPDR_PULLUP(GPIOC_ADC12_IN12) |  \
                                      PIN_PUPDR_PULLUP(GPIOC_ADC12_IN13) |  \
                                      PIN_PUPDR_FLOATING(GPIOC_DIR) |       \
                                      PIN_PUPDR_FLOATING(GPIOC_SLEEP ) |    \
                                      PIN_PUPDR_PULLUP(GPIOC_PIN6) |        \
                                      PIN_PUPDR_PULLUP(GPIOC_PIN7 ) |       \
                                      PIN_PUPDR_PULLUP(GPIOC_PIN8) |        \
                                      PIN_PUPDR_PULLUP(GPIOC_PIN9) |        \
                                      PIN_PUPDR_FLOATING(GPIOC_STM_TX) |    \
                                      PIN_PUPDR_FLOATING(GPIOC_STM_RX ) |   \
                                      PIN_PUPDR_PULLUP(GPIOC_PIN12) |       \
                                      PIN_PUPDR_PULLUP(GPIOC_PIN13) |       \
                                      PIN_PUPDR_FLOATING(GPIOC_OSC32_IN) |  \
                                      PIN_PUPDR_FLOATING(GPIOC_OSC32_OUT))
 #define VAL_GPIOC_ODR               (PIN_ODR_HIGH(GPIOC_PIN0) |            \
                                      PIN_ODR_HIGH(GPIOC_PIN1) |            \
                                      PIN_ODR_HIGH(GPIOC_ADC12_IN12) |      \
                                      PIN_ODR_HIGH(GPIOC_ADC12_IN13) |      \
                                      PIN_ODR_HIGH(GPIOC_DIR) |             \
                                      PIN_ODR_HIGH(GPIOC_SLEEP ) |          \
                                      PIN_ODR_HIGH(GPIOC_PIN6) |            \
                                      PIN_ODR_HIGH(GPIOC_PIN7 ) |           \
                                      PIN_ODR_HIGH(GPIOC_PIN8) |            \
                                      PIN_ODR_HIGH(GPIOC_PIN9) |            \
                                      PIN_ODR_HIGH(GPIOC_STM_TX) |          \
                                      PIN_ODR_HIGH(GPIOC_STM_RX ) |         \
                                      PIN_ODR_HIGH(GPIOC_PIN12) |           \
                                      PIN_ODR_HIGH(GPIOC_PIN13) |           \
                                      PIN_ODR_HIGH(GPIOC_OSC32_IN) |        \
                                      PIN_ODR_HIGH(GPIOC_OSC32_OUT))
 #define VAL_GPIOC_AFRL              (PIN_AFIO_AF(GPIOC_PIN0, 0U) |         \
                                      PIN_AFIO_AF(GPIOC_PIN1, 0U) |         \
                                      PIN_AFIO_AF(GPIOC_ADC12_IN12, 0U) |   \
                                      PIN_AFIO_AF(GPIOC_ADC12_IN13, 0U) |   \
                                      PIN_AFIO_AF(GPIOC_DIR, 0U) |          \
                                      PIN_AFIO_AF(GPIOC_SLEEP , 0U) |       \
                                      PIN_AFIO_AF(GPIOC_PIN6, 0U) |         \
                                      PIN_AFIO_AF(GPIOC_PIN7 , 0U))
 #define VAL_GPIOC_AFRH              (PIN_AFIO_AF(GPIOC_PIN8, 0U) |         \
                                      PIN_AFIO_AF(GPIOC_PIN9, 0U) |         \
                                      PIN_AFIO_AF(GPIOC_STM_TX, 7U) |       \
                                      PIN_AFIO_AF(GPIOC_STM_RX , 7U) |      \
                                      PIN_AFIO_AF(GPIOC_PIN12, 0U) |        \
                                      PIN_AFIO_AF(GPIOC_PIN13, 0U) |        \
                                      PIN_AFIO_AF(GPIOC_OSC32_IN, 0U) |     \
                                      PIN_AFIO_AF(GPIOC_OSC32_OUT, 0U))

/*
 * GPIOD setup:
 *
 * PD0  - ZIO_D67 CAN1_RX           (input pullup).
 * PD1  - ZIO_D66 CAN1_TX           (input pullup).
 * PD2  - ZIO_D48 SDMMC_CMD         (input pullup).
 * PD3  - ZIO_D55 USART2_CTS        (input pullup).
 * PD4  - ZIO_D54 USART2_RTS        (input pullup).
 * PD5  - ZIO_D53 USART2_TX         (input pullup).
 * PD6  - ZIO_D52 USART2_RX         (input pullup).
 * PD7  - ZIO_D51 USART2_SCLK       (input pullup).
 * PD8  - USART3_RX STLK_RX         (alternate 7).
 * PD9  - USART3_TX STLK_TX         (alternate 7).
 * PD10 - PIN10                     (input pullup).
 * PD11 - ZIO_D30 QUADSPI_BK1_IO0   (input pullup).
 * PD12 - ZIO_D29 QUADSPI_BK1_IO1   (input pullup).
 * PD13 - ZIO_D28 QUADSPI_BK1_IO3   (input pullup).
 * PD14 - ARD_D10 SPI1_NSS          (input pullup).
 * PD15 - ARD_D9 TIM4_CH4           (input pullup).
 */
 #define VAL_GPIOD_MODER             (PIN_MODE_ALTERNATE(GPIOD_FMC_D2) |    \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_D3) |    \
                                      PIN_MODE_ALTERNATE(GPIOD_PIN2) |          \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_CLK) |   \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_NOE) |   \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_NWE) |   \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_NWAIT) | \
                                      PIN_MODE_INPUT(GPIOD_PIN7) |          \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_D13) |   \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_D14) |   \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_D15) |   \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_A16) |   \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_A17) |   \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_A18) |   \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_D0) |    \
                                      PIN_MODE_ALTERNATE(GPIOD_FMC_D1))
 #define VAL_GPIOD_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOD_FMC_D2) |    \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_D3) |    \
                                      PIN_OTYPE_PUSHPULL(GPIOD_PIN2) |      \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_CLK) |   \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_NOE) |   \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_NWE) |   \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_NWAIT) | \
                                      PIN_OTYPE_PUSHPULL(GPIOD_PIN7) |      \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_D13 ) |  \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_D14 ) |  \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_D15) |   \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_A16) |   \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_A17) |   \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_A18) |   \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_D0) |    \
                                      PIN_OTYPE_PUSHPULL(GPIOD_FMC_D1))
 #define VAL_GPIOD_OSPEEDR           (PIN_OSPEED_HIGH(GPIOD_FMC_D2) |       \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_D3) |       \
                                      PIN_OSPEED_HIGH(GPIOD_PIN2) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_CLK) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_NOE) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_NWE) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_NWAIT) |    \
                                      PIN_OSPEED_VERYLOW(GPIOD_PIN7) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_D13) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_D14) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_D15) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_A16) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_A17) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_A18) |      \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_D0) |       \
                                      PIN_OSPEED_HIGH(GPIOD_FMC_D1))
 #define VAL_GPIOD_PUPDR             (PIN_PUPDR_FLOATING(GPIOD_FMC_D2) |    \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_D3) |    \
                                      PIN_PUPDR_FLOATING(GPIOD_PIN2) |        \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_CLK) |   \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_NOE) |   \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_NWE) |   \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_NWAIT) | \
                                      PIN_PUPDR_PULLUP(GPIOD_PIN7) |        \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_D13) |   \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_D14) |   \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_D15) |   \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_A16) |   \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_A17) |   \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_A18) |   \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_D0) |    \
                                      PIN_PUPDR_FLOATING(GPIOD_FMC_D1))
 #define VAL_GPIOD_ODR               (PIN_ODR_LOW(GPIOD_FMC_D2) |           \
                                      PIN_ODR_LOW(GPIOD_FMC_D3) |           \
                                      PIN_ODR_HIGH(GPIOD_PIN2) |            \
                                      PIN_ODR_LOW(GPIOD_FMC_CLK) |          \
                                      PIN_ODR_LOW(GPIOD_FMC_NOE) |          \
                                      PIN_ODR_LOW(GPIOD_FMC_NWE) |          \
                                      PIN_ODR_LOW(GPIOD_FMC_NWAIT) |        \
                                      PIN_ODR_HIGH(GPIOD_PIN7) |            \
                                      PIN_ODR_LOW(GPIOD_FMC_D13 ) |         \
                                      PIN_ODR_LOW(GPIOD_FMC_D14 ) |         \
                                      PIN_ODR_LOW(GPIOD_FMC_D15) |          \
                                      PIN_ODR_LOW(GPIOD_FMC_A16) |          \
                                      PIN_ODR_LOW(GPIOD_FMC_A17) |          \
                                      PIN_ODR_LOW(GPIOD_FMC_A18) |          \
                                      PIN_ODR_LOW(GPIOD_FMC_D0) |           \
                                      PIN_ODR_LOW(GPIOD_FMC_D1))
 #define VAL_GPIOD_AFRL              (PIN_AFIO_AF(GPIOD_FMC_D2, 10U) |      \
                                      PIN_AFIO_AF(GPIOD_FMC_D3, 10U) |      \
                                      PIN_AFIO_AF(GPIOD_PIN2, 10U) |         \
                                      PIN_AFIO_AF(GPIOD_FMC_CLK, 10U) |     \
                                      PIN_AFIO_AF(GPIOD_FMC_NOE, 10U) |     \
                                      PIN_AFIO_AF(GPIOD_FMC_NWE, 10U) |     \
                                      PIN_AFIO_AF(GPIOD_FMC_NWAIT, 10U) |   \
                                      PIN_AFIO_AF(GPIOD_PIN7, 0U))
 #define VAL_GPIOD_AFRH              (PIN_AFIO_AF(GPIOD_FMC_D13 , 10U) |    \
                                      PIN_AFIO_AF(GPIOD_FMC_D14 , 10U) |    \
                                      PIN_AFIO_AF(GPIOD_FMC_D15, 10U) |     \
                                      PIN_AFIO_AF(GPIOD_FMC_A16, 10U) |     \
                                      PIN_AFIO_AF(GPIOD_FMC_A17, 10U) |     \
                                      PIN_AFIO_AF(GPIOD_FMC_A18, 10U) |     \
                                      PIN_AFIO_AF(GPIOD_FMC_D0, 10U) |      \
                                      PIN_AFIO_AF(GPIOD_FMC_D1, 10U))

/*
 * GPIOE setup:
 *
 * PE0  - FMC_NBL0                  (alternate 10).
 * PE1  - FMC_NBL1                  (alternate 10).
 * PE2  - FMC_A23                   (alternate 10).
 * PE3  - FMC_A19                   (alternate 10).
 * PE4  - FMC_A20                   (alternate 10).
 * PE5  - FMC_A21                   (alternate 10).
 * PE6  - FMC_A22                   (alternate 10).
 * PE7  - FMC_D4                    (alternate 10).
 * PE8  - FMC_D5                    (alternate 10).
 * PE9  - FMC_D6                    (alternate 10).
 * PE10 - FMC_D7                    (alternate 10).
 * PE11 - FMC_D8                    (alternate 10).
 * PE12 - FMC_D9                    (alternate 10).
 * PE13 - FMC_D10                   (alternate 10).
 * PE14 - FMC_D11                   (alternate 10).
 * PE15 - FMC_D12                   (alternate 10).
 */
#define VAL_GPIOE_MODER             (PIN_MODE_ALTERNATE(GPIOE_FMC_NBL0) |   \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_NBL1) |   \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_A23) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_A19) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_A20) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_A21) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_A22) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_D4) |     \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_D5) |     \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_D6) |     \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_D7) |     \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_D8) |     \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_D9) |     \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_D10) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_D11) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FMC_D12))
#define VAL_GPIOE_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOE_FMC_NBL0) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_NBL1) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_A23) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_A19) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_A20) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_A21) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_A22) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_D4) |     \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_D5) |     \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_D6) |     \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_D7) |     \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_D8) |     \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_D9) |     \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_D10) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_D11) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FMC_D12))
#define VAL_GPIOE_OSPEEDR           (PIN_OSPEED_HIGH(GPIOE_FMC_NBL0) |      \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_NBL1) |      \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_A23) |       \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_A19) |       \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_A20) |       \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_A21) |       \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_A22) |       \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_D4) |        \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_D5) |        \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_D6) |        \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_D7) |        \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_D8) |        \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_D9) |        \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_D10) |       \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_D11) |       \
                                     PIN_OSPEED_HIGH(GPIOE_FMC_D12))
#define VAL_GPIOE_PUPDR             (PIN_PUPDR_FLOATING(GPIOE_FMC_NBL0) |   \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_NBL1) |   \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_A23) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_A19) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_A20) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_A21) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_A22) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_D4) |     \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_D5) |     \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_D6) |     \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_D7) |     \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_D8) |     \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_D9) |     \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_D10) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_D11) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FMC_D12))
#define VAL_GPIOE_ODR               (PIN_ODR_LOW(GPIOE_FMC_NBL0) |          \
                                     PIN_ODR_LOW(GPIOE_FMC_NBL1) |          \
                                     PIN_ODR_LOW(GPIOE_FMC_A23) |           \
                                     PIN_ODR_LOW(GPIOE_FMC_A19) |           \
                                     PIN_ODR_LOW(GPIOE_FMC_A20) |           \
                                     PIN_ODR_LOW(GPIOE_FMC_A21) |           \
                                     PIN_ODR_LOW(GPIOE_FMC_A22) |           \
                                     PIN_ODR_LOW(GPIOE_FMC_D4) |            \
                                     PIN_ODR_LOW(GPIOE_FMC_D5) |            \
                                     PIN_ODR_LOW(GPIOE_FMC_D6) |            \
                                     PIN_ODR_LOW(GPIOE_FMC_D7) |            \
                                     PIN_ODR_LOW(GPIOE_FMC_D8) |            \
                                     PIN_ODR_LOW(GPIOE_FMC_D9) |            \
                                     PIN_ODR_LOW(GPIOE_FMC_D10) |           \
                                     PIN_ODR_LOW(GPIOE_FMC_D11) |           \
                                     PIN_ODR_LOW(GPIOE_FMC_D12))
#define VAL_GPIOE_AFRL              (PIN_AFIO_AF(GPIOE_FMC_NBL0, 10U) |     \
                                     PIN_AFIO_AF(GPIOE_FMC_NBL1, 10U) |     \
                                     PIN_AFIO_AF(GPIOE_FMC_A23, 10U) |      \
                                     PIN_AFIO_AF(GPIOE_FMC_A19, 10U) |      \
                                     PIN_AFIO_AF(GPIOE_FMC_A20, 10U) |      \
                                     PIN_AFIO_AF(GPIOE_FMC_A21, 10U) |      \
                                     PIN_AFIO_AF(GPIOE_FMC_A22, 10U) |      \
                                     PIN_AFIO_AF(GPIOE_FMC_D4, 10U))
#define VAL_GPIOE_AFRH              (PIN_AFIO_AF(GPIOE_FMC_D5, 10U) |       \
                                     PIN_AFIO_AF(GPIOE_FMC_D6, 10U) |       \
                                     PIN_AFIO_AF(GPIOE_FMC_D7, 10U) |       \
                                     PIN_AFIO_AF(GPIOE_FMC_D8, 10U) |       \
                                     PIN_AFIO_AF(GPIOE_FMC_D9, 10U) |       \
                                     PIN_AFIO_AF(GPIOE_FMC_D10, 10U) |      \
                                     PIN_AFIO_AF(GPIOE_FMC_D11, 10U) |      \
                                     PIN_AFIO_AF(GPIOE_FMC_D12, 10U))

/*
 * GPIOF setup:
 *
 * PF0  - ZIO_D68 I2C2_SDA          (input pullup).
 * PF1  - ZIO_D69 I2C2_SCL          (input pullup).
 * PF2  - ZIO_D70 I2C2_SMBA         (input pullup).
 * PF3  - ARD_A3 ADC3_IN9           (input pullup).
 * PF4  - ZIO_A8 ADC3_IN14          (input pullup).
 * PF5  - ARD_A4 ADC3_IN15          (input pullup).
 * PF6  - PIN6                      (input pullup).
 * PF7  - ZIO_D62 SAI1_MCLK_B       (input pullup).
 * PF8  - ZIO_D61 SAI1_SCK_B        (input pullup).
 * PF9  - ZIO_D63 SAI1_FS_B         (input pullup).
 * PF10 - ARD_A5 ADC3_IN8           (input pullup).
 * PF11 - PIN11                     (input pullup).
 * PF12 - ARD_D8                    (input pullup).
 * PF13 - ARD_D7                    (input pullup).
 * PF14 - ARD_D4                    (input pullup).
 * PF15 - ARD_D2                    (input pullup).
 */
 #define VAL_GPIOF_MODER             (PIN_MODE_ALTERNATE(GPIOF_I2C2_SDA) |  \
                                      PIN_MODE_ALTERNATE(GPIOF_I2C2_SCL) |  \
                                      PIN_MODE_INPUT(GPIOF_PIN2) |          \
                                      PIN_MODE_INPUT(GPIOF_PIN3) |          \
                                      PIN_MODE_INPUT(GPIOF_PIN4) |          \
                                      PIN_MODE_ANALOG(GPIOF_ADC3_IN15) |    \
                                      PIN_MODE_OUTPUT(GPIOF_DIV_CLK) |      \
                                      PIN_MODE_ANALOG(GPIOF_ADC3_IN4) |     \
                                      PIN_MODE_ANALOG(GPIOF_ADC3_IN5) |     \
                                      PIN_MODE_ANALOG(GPIOF_ADC3_IN6) |     \
                                      PIN_MODE_INPUT(GPIOF_PIN10) |         \
                                      PIN_MODE_INPUT(GPIOF_PIN11) |         \
                                      PIN_MODE_INPUT(GPIOF_PIN12) |         \
                                      PIN_MODE_INPUT(GPIOF_PIN13) |         \
                                      PIN_MODE_INPUT(GPIOF_PIN14) |         \
                                      PIN_MODE_INPUT(GPIOF_PIN15))
 #define VAL_GPIOF_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOF_I2C2_SDA) |  \
                                      PIN_OTYPE_PUSHPULL(GPIOF_I2C2_SCL) |  \
                                      PIN_OTYPE_PUSHPULL(GPIOF_PIN2) |      \
                                      PIN_OTYPE_PUSHPULL(GPIOF_PIN3) |      \
                                      PIN_OTYPE_PUSHPULL(GPIOF_PIN4) |      \
                                      PIN_OTYPE_PUSHPULL(GPIOF_ADC3_IN15) | \
                                      PIN_OTYPE_PUSHPULL(GPIOF_DIV_CLK) |   \
                                      PIN_OTYPE_PUSHPULL(GPIOF_ADC3_IN4) |  \
                                      PIN_OTYPE_PUSHPULL(GPIOF_ADC3_IN5) |  \
                                      PIN_OTYPE_PUSHPULL(GPIOF_ADC3_IN6) |  \
                                      PIN_OTYPE_PUSHPULL(GPIOF_PIN10) |     \
                                      PIN_OTYPE_PUSHPULL(GPIOF_PIN11) |     \
                                      PIN_OTYPE_PUSHPULL(GPIOF_PIN12) |     \
                                      PIN_OTYPE_PUSHPULL(GPIOF_PIN13) |     \
                                      PIN_OTYPE_PUSHPULL(GPIOF_PIN14) |     \
                                      PIN_OTYPE_PUSHPULL(GPIOF_PIN15))
 #define VAL_GPIOF_OSPEEDR           (PIN_OSPEED_HIGH(GPIOF_I2C2_SDA) |     \
                                      PIN_OSPEED_HIGH(GPIOF_I2C2_SCL) |     \
                                      PIN_OSPEED_VERYLOW(GPIOF_PIN2) |      \
                                      PIN_OSPEED_VERYLOW(GPIOF_PIN3) |      \
                                      PIN_OSPEED_VERYLOW(GPIOF_PIN4) |      \
                                      PIN_OSPEED_HIGH(GPIOF_ADC3_IN15) |    \
                                      PIN_OSPEED_HIGH(GPIOF_DIV_CLK) |      \
                                      PIN_OSPEED_HIGH(GPIOF_ADC3_IN4) |     \
                                      PIN_OSPEED_HIGH(GPIOF_ADC3_IN5) |     \
                                      PIN_OSPEED_HIGH(GPIOF_ADC3_IN6) |     \
                                      PIN_OSPEED_HIGH(GPIOF_PIN10) |        \
                                      PIN_OSPEED_VERYLOW(GPIOF_PIN11) |     \
                                      PIN_OSPEED_VERYLOW(GPIOF_PIN12) |     \
                                      PIN_OSPEED_VERYLOW(GPIOF_PIN13) |     \
                                      PIN_OSPEED_VERYLOW(GPIOF_PIN14) |     \
                                      PIN_OSPEED_VERYLOW(GPIOF_PIN15))
 #define VAL_GPIOF_PUPDR             (PIN_PUPDR_PULLUP(GPIOF_I2C2_SDA) |    \
                                      PIN_PUPDR_PULLUP(GPIOF_I2C2_SCL) |    \
                                      PIN_PUPDR_PULLUP(GPIOF_PIN2) |        \
                                      PIN_PUPDR_PULLUP(GPIOF_PIN3) |        \
                                      PIN_PUPDR_PULLUP(GPIOF_PIN4) |        \
                                      PIN_PUPDR_PULLUP(GPIOF_ADC3_IN15) |   \
                                      PIN_PUPDR_FLOATING(GPIOF_DIV_CLK) |   \
                                      PIN_PUPDR_PULLUP(GPIOF_ADC3_IN4) |    \
                                      PIN_PUPDR_PULLUP(GPIOF_ADC3_IN5) |    \
                                      PIN_PUPDR_PULLUP(GPIOF_ADC3_IN6) |    \
                                      PIN_PUPDR_PULLUP(GPIOF_PIN10) |       \
                                      PIN_PUPDR_PULLUP(GPIOF_PIN11) |       \
                                      PIN_PUPDR_PULLUP(GPIOF_PIN12) |       \
                                      PIN_PUPDR_PULLUP(GPIOF_PIN13) |       \
                                      PIN_PUPDR_PULLUP(GPIOF_PIN14) |       \
                                      PIN_PUPDR_PULLUP(GPIOF_PIN15))
 #define VAL_GPIOF_ODR               (PIN_ODR_HIGH(GPIOF_I2C2_SDA) |        \
                                      PIN_ODR_HIGH(GPIOF_I2C2_SCL) |        \
                                      PIN_ODR_HIGH(GPIOF_PIN2) |            \
                                      PIN_ODR_HIGH(GPIOF_PIN3) |            \
                                      PIN_ODR_HIGH(GPIOF_PIN4) |            \
                                      PIN_ODR_HIGH(GPIOF_ADC3_IN15) |       \
                                      PIN_ODR_LOW(GPIOF_DIV_CLK) |          \
                                      PIN_ODR_HIGH(GPIOF_ADC3_IN4) |        \
                                      PIN_ODR_HIGH(GPIOF_ADC3_IN5) |        \
                                      PIN_ODR_HIGH(GPIOF_ADC3_IN6) |        \
                                      PIN_ODR_HIGH(GPIOF_PIN10) |           \
                                      PIN_ODR_HIGH(GPIOF_PIN11) |           \
                                      PIN_ODR_HIGH(GPIOF_PIN12) |           \
                                      PIN_ODR_HIGH(GPIOF_PIN13) |           \
                                      PIN_ODR_HIGH(GPIOF_PIN14) |           \
                                      PIN_ODR_HIGH(GPIOF_PIN15))
 #define VAL_GPIOF_AFRL              (PIN_AFIO_AF(GPIOF_I2C2_SDA, 0U) |     \
                                      PIN_AFIO_AF(GPIOF_I2C2_SCL, 0U) |     \
                                      PIN_AFIO_AF(GPIOF_PIN2, 0U) |         \
                                      PIN_AFIO_AF(GPIOF_PIN3, 0U) |         \
                                      PIN_AFIO_AF(GPIOF_PIN4, 0U) |         \
                                      PIN_AFIO_AF(GPIOF_ADC3_IN15, 0U) |    \
                                      PIN_AFIO_AF(GPIOF_DIV_CLK, 0U) |      \
                                      PIN_AFIO_AF(GPIOF_ADC3_IN4, 0U))
 #define VAL_GPIOF_AFRH              (PIN_AFIO_AF(GPIOF_ADC3_IN5, 0U) |     \
                                      PIN_AFIO_AF(GPIOF_ADC3_IN6, 0U) |     \
                                      PIN_AFIO_AF(GPIOF_PIN10, 0U) |        \
                                      PIN_AFIO_AF(GPIOF_PIN11, 0U) |        \
                                      PIN_AFIO_AF(GPIOF_PIN12, 0U) |        \
                                      PIN_AFIO_AF(GPIOF_PIN13, 0U) |        \
                                      PIN_AFIO_AF(GPIOF_PIN14, 0U) |        \
                                      PIN_AFIO_AF(GPIOF_PIN15, 0U))

/*
 * GPIOG setup:
 *
 * PG0  - PIN0                      (input pullup).
 * PG1  - PIN1                      (input pullup).
 * PG2  - PIN2                      (input pullup).
 * PG3  - PIN3                      (input pullup).
 * PG4  - LED2                      (output pushpull maximum).
 * PG5  - PIN5                      (input pullup).
 * PG6  - PIN6                      (input pullup).
 * PG7  - PIN7                      (input pullup).
 * PG8  - PIN8                      (input pullup).
 * PG9  - FMC_NE2                   (alternate 10).
 * PG10 - PIN10                     (input pullup).
 * PG11 - PIN11                     (input pullup).
 * PG12 - PIN12                     (input pullup).
 * PG13 - FMC_A24                   (alternate 10).
 * PG14 - FMC_A25                   (alternate 10).
 * PG15 - PIN15                     (input pullup).
 */
#define VAL_GPIOG_MODER             (PIN_MODE_INPUT(GPIOG_PIN0) |           \
                                     PIN_MODE_INPUT(GPIOG_PIN1) |           \
                                     PIN_MODE_INPUT(GPIOG_PIN2) |           \
                                     PIN_MODE_INPUT(GPIOG_PIN3) |           \
                                     PIN_MODE_OUTPUT(GPIOG_LED2) |          \
                                     PIN_MODE_INPUT(GPIOG_PIN5) |           \
                                     PIN_MODE_INPUT(GPIOG_PIN6) |           \
                                     PIN_MODE_INPUT(GPIOG_PIN7) |           \
                                     PIN_MODE_INPUT(GPIOG_PIN8) |           \
                                     PIN_MODE_ALTERNATE(GPIOG_FMC_NE2) |    \
                                     PIN_MODE_INPUT(GPIOG_PIN10) |          \
                                     PIN_MODE_INPUT(GPIOG_PIN11) |          \
                                     PIN_MODE_INPUT(GPIOG_PIN12) |          \
                                     PIN_MODE_ALTERNATE(GPIOG_FMC_A24) |    \
                                     PIN_MODE_ALTERNATE(GPIOG_FMC_A25) |    \
                                     PIN_MODE_INPUT(GPIOG_PIN15))
#define VAL_GPIOG_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOG_PIN0) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_LED2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN5) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN7) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN8) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_FMC_NE2) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN11) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN12) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOG_FMC_A24) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOG_FMC_A25) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN15))
#define VAL_GPIOG_OSPEEDR           (PIN_OSPEED_VERYLOW(GPIOG_PIN0) |       \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN1) |       \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN2) |       \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN3) |       \
                                     PIN_OSPEED_HIGH(GPIOG_LED2) |          \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN5) |       \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN6) |       \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN7) |       \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN8) |       \
                                     PIN_OSPEED_HIGH(GPIOG_FMC_NE2) |       \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN10) |      \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN11) |      \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN12) |      \
                                     PIN_OSPEED_HIGH(GPIOG_FMC_A24) |       \
                                     PIN_OSPEED_HIGH(GPIOG_FMC_A25) |       \
                                     PIN_OSPEED_VERYLOW(GPIOG_PIN15))
#define VAL_GPIOG_PUPDR             (PIN_PUPDR_PULLUP(GPIOG_PIN0) |         \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN1) |         \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN2) |         \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN3) |         \
                                     PIN_PUPDR_FLOATING(GPIOG_LED2) |       \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN5) |         \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN6) |         \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN7) |         \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN8) |         \
                                     PIN_PUPDR_FLOATING(GPIOG_FMC_NE2) |    \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN10) |        \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN11) |        \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN12) |        \
                                     PIN_PUPDR_FLOATING(GPIOG_FMC_A24) |    \
                                     PIN_PUPDR_FLOATING(GPIOG_FMC_A25) |    \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN15))
#define VAL_GPIOG_ODR               (PIN_ODR_HIGH(GPIOG_PIN0) |             \
                                     PIN_ODR_HIGH(GPIOG_PIN1) |             \
                                     PIN_ODR_HIGH(GPIOG_PIN2) |             \
                                     PIN_ODR_HIGH(GPIOG_PIN3) |             \
                                     PIN_ODR_LOW(GPIOG_LED2) |              \
                                     PIN_ODR_HIGH(GPIOG_PIN5) |             \
                                     PIN_ODR_HIGH(GPIOG_PIN6) |             \
                                     PIN_ODR_HIGH(GPIOG_PIN7) |             \
                                     PIN_ODR_HIGH(GPIOG_PIN8) |             \
                                     PIN_ODR_LOW(GPIOG_FMC_NE2) |           \
                                     PIN_ODR_HIGH(GPIOG_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOG_PIN11) |            \
                                     PIN_ODR_HIGH(GPIOG_PIN12) |            \
                                     PIN_ODR_LOW(GPIOG_FMC_A24) |           \
                                     PIN_ODR_LOW(GPIOG_FMC_A25) |           \
                                     PIN_ODR_HIGH(GPIOG_PIN15))
#define VAL_GPIOG_AFRL              (PIN_AFIO_AF(GPIOG_PIN0, 0U) |          \
                                     PIN_AFIO_AF(GPIOG_PIN1, 0U) |          \
                                     PIN_AFIO_AF(GPIOG_PIN2, 0U) |          \
                                     PIN_AFIO_AF(GPIOG_PIN3, 0U) |          \
                                     PIN_AFIO_AF(GPIOG_LED2, 0U) |          \
                                     PIN_AFIO_AF(GPIOG_PIN5, 0U) |          \
                                     PIN_AFIO_AF(GPIOG_PIN6, 0U) |          \
                                     PIN_AFIO_AF(GPIOG_PIN7, 0U))
#define VAL_GPIOG_AFRH              (PIN_AFIO_AF(GPIOG_PIN8, 0U) |          \
                                     PIN_AFIO_AF(GPIOG_FMC_NE2, 10U) |      \
                                     PIN_AFIO_AF(GPIOG_PIN10, 0U) |         \
                                     PIN_AFIO_AF(GPIOG_PIN11, 0U) |         \
                                     PIN_AFIO_AF(GPIOG_PIN12, 0U) |         \
                                     PIN_AFIO_AF(GPIOG_FMC_A24, 10U) |      \
                                     PIN_AFIO_AF(GPIOG_FMC_A25, 10U) |      \
                                     PIN_AFIO_AF(GPIOG_PIN15, 0U))

/*
 * GPIOH setup:
 *
 * PH0  - OSC_IN                    (input floating).
 * PH1  - OSC_OUT                   (input floating).
 * PH2  - PIN2                      (input pullup).
 * PH3  - PIN3                      (input pullup).
 * PH4  - PIN4                      (input pullup).
 * PH5  - PIN5                      (input pullup).
 * PH6  - PIN6                      (input pullup).
 * PH7  - PIN7                      (input pullup).
 * PH8  - PIN8                      (input pullup).
 * PH9  - PIN9                      (input pullup).
 * PH10 - PIN10                     (input pullup).
 * PH11 - PIN11                     (input pullup).
 * PH12 - PIN12                     (input pullup).
 * PH13 - PIN13                     (input pullup).
 * PH14 - PIN14                     (input pullup).
 * PH15 - PIN15                     (input pullup).
 */
#define VAL_GPIOH_MODER             (PIN_MODE_INPUT(GPIOH_OSC_IN) |         \
                                     PIN_MODE_INPUT(GPIOH_OSC_OUT) |        \
                                     PIN_MODE_INPUT(GPIOH_PIN2) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN3) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN4) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN5) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN6) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN7) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN8) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN9) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN10) |          \
                                     PIN_MODE_INPUT(GPIOH_PIN11) |          \
                                     PIN_MODE_INPUT(GPIOH_PIN12) |          \
                                     PIN_MODE_INPUT(GPIOH_PIN13) |          \
                                     PIN_MODE_INPUT(GPIOH_PIN14) |          \
                                     PIN_MODE_INPUT(GPIOH_PIN15))
#define VAL_GPIOH_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOH_OSC_IN) |     \
                                     PIN_OTYPE_PUSHPULL(GPIOH_OSC_OUT) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN4) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN5) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN7) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN8) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN9) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN11) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN12) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN13) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN14) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN15))
#define VAL_GPIOH_OSPEEDR           (PIN_OSPEED_HIGH(GPIOH_OSC_IN) |        \
                                     PIN_OSPEED_HIGH(GPIOH_OSC_OUT) |       \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN2) |       \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN3) |       \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN4) |       \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN5) |       \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN6) |       \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN7) |       \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN8) |       \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN9) |       \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN10) |      \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN11) |      \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN12) |      \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN13) |      \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN14) |      \
                                     PIN_OSPEED_VERYLOW(GPIOH_PIN15))
#define VAL_GPIOH_PUPDR             (PIN_PUPDR_FLOATING(GPIOH_OSC_IN) |     \
                                     PIN_PUPDR_FLOATING(GPIOH_OSC_OUT) |    \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN2) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN3) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN4) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN5) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN6) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN7) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN8) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN9) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN10) |        \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN11) |        \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN12) |        \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN13) |        \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN14) |        \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN15))
#define VAL_GPIOH_ODR               (PIN_ODR_HIGH(GPIOH_OSC_IN) |           \
                                     PIN_ODR_HIGH(GPIOH_OSC_OUT) |          \
                                     PIN_ODR_HIGH(GPIOH_PIN2) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN3) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN4) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN5) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN6) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN7) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN8) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN9) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOH_PIN11) |            \
                                     PIN_ODR_HIGH(GPIOH_PIN12) |            \
                                     PIN_ODR_HIGH(GPIOH_PIN13) |            \
                                     PIN_ODR_HIGH(GPIOH_PIN14) |            \
                                     PIN_ODR_HIGH(GPIOH_PIN15))
#define VAL_GPIOH_AFRL              (PIN_AFIO_AF(GPIOH_OSC_IN, 0U) |        \
                                     PIN_AFIO_AF(GPIOH_OSC_OUT, 0U) |       \
                                     PIN_AFIO_AF(GPIOH_PIN2, 0U) |          \
                                     PIN_AFIO_AF(GPIOH_PIN3, 0U) |          \
                                     PIN_AFIO_AF(GPIOH_PIN4, 0U) |          \
                                     PIN_AFIO_AF(GPIOH_PIN5, 0U) |          \
                                     PIN_AFIO_AF(GPIOH_PIN6, 0U) |          \
                                     PIN_AFIO_AF(GPIOH_PIN7, 0U))
#define VAL_GPIOH_AFRH              (PIN_AFIO_AF(GPIOH_PIN8, 0U) |          \
                                     PIN_AFIO_AF(GPIOH_PIN9, 0U) |          \
                                     PIN_AFIO_AF(GPIOH_PIN10, 0U) |         \
                                     PIN_AFIO_AF(GPIOH_PIN11, 0U) |         \
                                     PIN_AFIO_AF(GPIOH_PIN12, 0U) |         \
                                     PIN_AFIO_AF(GPIOH_PIN13, 0U) |         \
                                     PIN_AFIO_AF(GPIOH_PIN14, 0U) |         \
                                     PIN_AFIO_AF(GPIOH_PIN15, 0U))

/*
 * GPIOI setup:
 *
 * PI0  - PIN0                      (input pullup).
 * PI1  - PIN1                      (input pullup).
 * PI2  - PIN2                      (input pullup).
 * PI3  - PIN3                      (input pullup).
 * PI4  - PIN4                      (input pullup).
 * PI5  - PIN5                      (input pullup).
 * PI6  - PIN6                      (input pullup).
 * PI7  - PIN7                      (input pullup).
 * PI8  - PIN8                      (input pullup).
 * PI9  - PIN9                      (input pullup).
 * PI10 - PIN10                     (input pullup).
 * PI11 - PIN11                     (input pullup).
 * PI12 - PIN12                     (input pullup).
 * PI13 - PIN13                     (input pullup).
 * PI14 - PIN14                     (input pullup).
 * PI15 - PIN15                     (input pullup).
 */
#define VAL_GPIOI_MODER             (PIN_MODE_INPUT(GPIOI_PIN0) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN1) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN2) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN3) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN4) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN5) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN6) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN7) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN8) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN9) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN10) |          \
                                     PIN_MODE_INPUT(GPIOI_PIN11) |          \
                                     PIN_MODE_INPUT(GPIOI_PIN12) |          \
                                     PIN_MODE_INPUT(GPIOI_PIN13) |          \
                                     PIN_MODE_INPUT(GPIOI_PIN14) |          \
                                     PIN_MODE_INPUT(GPIOI_PIN15))
#define VAL_GPIOI_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOI_PIN0) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN4) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN5) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN7) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN8) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN9) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN11) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN12) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN13) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN14) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN15))
#define VAL_GPIOI_OSPEEDR           (PIN_OSPEED_VERYLOW(GPIOI_PIN0) |       \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN1) |       \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN2) |       \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN3) |       \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN4) |       \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN5) |       \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN6) |       \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN7) |       \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN8) |       \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN9) |       \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN10) |      \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN11) |      \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN12) |      \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN13) |      \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN14) |      \
                                     PIN_OSPEED_VERYLOW(GPIOI_PIN15))
#define VAL_GPIOI_PUPDR             (PIN_PUPDR_PULLUP(GPIOI_PIN0) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN1) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN2) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN3) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN4) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN5) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN6) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN7) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN8) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN9) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN10) |        \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN11) |        \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN12) |        \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN13) |        \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN14) |        \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN15))
#define VAL_GPIOI_ODR               (PIN_ODR_HIGH(GPIOI_PIN0) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN1) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN2) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN3) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN4) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN5) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN6) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN7) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN8) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN9) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOI_PIN11) |            \
                                     PIN_ODR_HIGH(GPIOI_PIN12) |            \
                                     PIN_ODR_HIGH(GPIOI_PIN13) |            \
                                     PIN_ODR_HIGH(GPIOI_PIN14) |            \
                                     PIN_ODR_HIGH(GPIOI_PIN15))
#define VAL_GPIOI_AFRL              (PIN_AFIO_AF(GPIOI_PIN0, 0U) |          \
                                     PIN_AFIO_AF(GPIOI_PIN1, 0U) |          \
                                     PIN_AFIO_AF(GPIOI_PIN2, 0U) |          \
                                     PIN_AFIO_AF(GPIOI_PIN3, 0U) |          \
                                     PIN_AFIO_AF(GPIOI_PIN4, 0U) |          \
                                     PIN_AFIO_AF(GPIOI_PIN5, 0U) |          \
                                     PIN_AFIO_AF(GPIOI_PIN6, 0U) |          \
                                     PIN_AFIO_AF(GPIOI_PIN7, 0U))
#define VAL_GPIOI_AFRH              (PIN_AFIO_AF(GPIOI_PIN8, 0U) |          \
                                     PIN_AFIO_AF(GPIOI_PIN9, 0U) |          \
                                     PIN_AFIO_AF(GPIOI_PIN10, 0U) |         \
                                     PIN_AFIO_AF(GPIOI_PIN11, 0U) |         \
                                     PIN_AFIO_AF(GPIOI_PIN12, 0U) |         \
                                     PIN_AFIO_AF(GPIOI_PIN13, 0U) |         \
                                     PIN_AFIO_AF(GPIOI_PIN14, 0U) |         \
                                     PIN_AFIO_AF(GPIOI_PIN15, 0U))

/*
 * GPIOJ setup:
 *
 * PJ0  - PIN0                      (input pullup).
 * PJ1  - PIN1                      (input pullup).
 * PJ2  - PIN2                      (input pullup).
 * PJ3  - PIN3                      (input pullup).
 * PJ4  - PIN4                      (input pullup).
 * PJ5  - PIN5                      (input pullup).
 * PJ6  - PIN6                      (input pullup).
 * PJ7  - PIN7                      (input pullup).
 * PJ8  - PIN8                      (input pullup).
 * PJ9  - PIN9                      (input pullup).
 * PJ10 - PIN10                     (input pullup).
 * PJ11 - PIN11                     (input pullup).
 * PJ12 - PIN12                     (input pullup).
 * PJ13 - PIN13                     (input pullup).
 * PJ14 - PIN14                     (input pullup).
 * PJ15 - PIN15                     (input pullup).
 */
#define VAL_GPIOJ_MODER             (PIN_MODE_INPUT(GPIOJ_PIN0) |           \
                                     PIN_MODE_INPUT(GPIOJ_PIN1) |           \
                                     PIN_MODE_INPUT(GPIOJ_PIN2) |           \
                                     PIN_MODE_INPUT(GPIOJ_PIN3) |           \
                                     PIN_MODE_INPUT(GPIOJ_PIN4) |           \
                                     PIN_MODE_INPUT(GPIOJ_PIN5) |           \
                                     PIN_MODE_INPUT(GPIOJ_PIN6) |           \
                                     PIN_MODE_INPUT(GPIOJ_PIN7) |           \
                                     PIN_MODE_INPUT(GPIOJ_PIN8) |           \
                                     PIN_MODE_INPUT(GPIOJ_PIN9) |           \
                                     PIN_MODE_INPUT(GPIOJ_PIN10) |          \
                                     PIN_MODE_INPUT(GPIOJ_PIN11) |          \
                                     PIN_MODE_INPUT(GPIOJ_PIN12) |          \
                                     PIN_MODE_INPUT(GPIOJ_PIN13) |          \
                                     PIN_MODE_INPUT(GPIOJ_PIN14) |          \
                                     PIN_MODE_INPUT(GPIOJ_PIN15))
#define VAL_GPIOJ_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOJ_PIN0) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN4) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN5) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN7) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN8) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN9) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN11) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN12) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN13) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN14) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOJ_PIN15))
#define VAL_GPIOJ_OSPEEDR           (PIN_OSPEED_VERYLOW(GPIOJ_PIN0) |       \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN1) |       \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN2) |       \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN3) |       \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN4) |       \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN5) |       \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN6) |       \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN7) |       \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN8) |       \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN9) |       \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN10) |      \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN11) |      \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN12) |      \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN13) |      \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN14) |      \
                                     PIN_OSPEED_VERYLOW(GPIOJ_PIN15))
#define VAL_GPIOJ_PUPDR             (PIN_PUPDR_PULLUP(GPIOJ_PIN0) |         \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN1) |         \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN2) |         \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN3) |         \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN4) |         \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN5) |         \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN6) |         \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN7) |         \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN8) |         \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN9) |         \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN10) |        \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN11) |        \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN12) |        \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN13) |        \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN14) |        \
                                     PIN_PUPDR_PULLUP(GPIOJ_PIN15))
#define VAL_GPIOJ_ODR               (PIN_ODR_HIGH(GPIOJ_PIN0) |             \
                                     PIN_ODR_HIGH(GPIOJ_PIN1) |             \
                                     PIN_ODR_HIGH(GPIOJ_PIN2) |             \
                                     PIN_ODR_HIGH(GPIOJ_PIN3) |             \
                                     PIN_ODR_HIGH(GPIOJ_PIN4) |             \
                                     PIN_ODR_HIGH(GPIOJ_PIN5) |             \
                                     PIN_ODR_HIGH(GPIOJ_PIN6) |             \
                                     PIN_ODR_HIGH(GPIOJ_PIN7) |             \
                                     PIN_ODR_HIGH(GPIOJ_PIN8) |             \
                                     PIN_ODR_HIGH(GPIOJ_PIN9) |             \
                                     PIN_ODR_HIGH(GPIOJ_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOJ_PIN11) |            \
                                     PIN_ODR_HIGH(GPIOJ_PIN12) |            \
                                     PIN_ODR_HIGH(GPIOJ_PIN13) |            \
                                     PIN_ODR_HIGH(GPIOJ_PIN14) |            \
                                     PIN_ODR_HIGH(GPIOJ_PIN15))
#define VAL_GPIOJ_AFRL              (PIN_AFIO_AF(GPIOJ_PIN0, 0U) |          \
                                     PIN_AFIO_AF(GPIOJ_PIN1, 0U) |          \
                                     PIN_AFIO_AF(GPIOJ_PIN2, 0U) |          \
                                     PIN_AFIO_AF(GPIOJ_PIN3, 0U) |          \
                                     PIN_AFIO_AF(GPIOJ_PIN4, 0U) |          \
                                     PIN_AFIO_AF(GPIOJ_PIN5, 0U) |          \
                                     PIN_AFIO_AF(GPIOJ_PIN6, 0U) |          \
                                     PIN_AFIO_AF(GPIOJ_PIN7, 0U))
#define VAL_GPIOJ_AFRH              (PIN_AFIO_AF(GPIOJ_PIN8, 0U) |          \
                                     PIN_AFIO_AF(GPIOJ_PIN9, 0U) |          \
                                     PIN_AFIO_AF(GPIOJ_PIN10, 0U) |         \
                                     PIN_AFIO_AF(GPIOJ_PIN11, 0U) |         \
                                     PIN_AFIO_AF(GPIOJ_PIN12, 0U) |         \
                                     PIN_AFIO_AF(GPIOJ_PIN13, 0U) |         \
                                     PIN_AFIO_AF(GPIOJ_PIN14, 0U) |         \
                                     PIN_AFIO_AF(GPIOJ_PIN15, 0U))

/*
 * GPIOK setup:
 *
 * PK0  - PIN0                      (input pullup).
 * PK1  - PIN1                      (input pullup).
 * PK2  - PIN2                      (input pullup).
 * PK3  - PIN3                      (input pullup).
 * PK4  - PIN4                      (input pullup).
 * PK5  - PIN5                      (input pullup).
 * PK6  - PIN6                      (input pullup).
 * PK7  - PIN7                      (input pullup).
 * PK8  - PIN8                      (input pullup).
 * PK9  - PIN9                      (input pullup).
 * PK10 - PIN10                     (input pullup).
 * PK11 - PIN11                     (input pullup).
 * PK12 - PIN12                     (input pullup).
 * PK13 - PIN13                     (input pullup).
 * PK14 - PIN14                     (input pullup).
 * PK15 - PIN15                     (input pullup).
 */
#define VAL_GPIOK_MODER             (PIN_MODE_INPUT(GPIOK_PIN0) |           \
                                     PIN_MODE_INPUT(GPIOK_PIN1) |           \
                                     PIN_MODE_INPUT(GPIOK_PIN2) |           \
                                     PIN_MODE_INPUT(GPIOK_PIN3) |           \
                                     PIN_MODE_INPUT(GPIOK_PIN4) |           \
                                     PIN_MODE_INPUT(GPIOK_PIN5) |           \
                                     PIN_MODE_INPUT(GPIOK_PIN6) |           \
                                     PIN_MODE_INPUT(GPIOK_PIN7) |           \
                                     PIN_MODE_INPUT(GPIOK_PIN8) |           \
                                     PIN_MODE_INPUT(GPIOK_PIN9) |           \
                                     PIN_MODE_INPUT(GPIOK_PIN10) |          \
                                     PIN_MODE_INPUT(GPIOK_PIN11) |          \
                                     PIN_MODE_INPUT(GPIOK_PIN12) |          \
                                     PIN_MODE_INPUT(GPIOK_PIN13) |          \
                                     PIN_MODE_INPUT(GPIOK_PIN14) |          \
                                     PIN_MODE_INPUT(GPIOK_PIN15))
#define VAL_GPIOK_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOK_PIN0) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN4) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN5) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN7) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN8) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN9) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN11) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN12) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN13) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN14) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOK_PIN15))
#define VAL_GPIOK_OSPEEDR           (PIN_OSPEED_VERYLOW(GPIOK_PIN0) |       \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN1) |       \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN2) |       \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN3) |       \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN4) |       \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN5) |       \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN6) |       \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN7) |       \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN8) |       \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN9) |       \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN10) |      \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN11) |      \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN12) |      \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN13) |      \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN14) |      \
                                     PIN_OSPEED_VERYLOW(GPIOK_PIN15))
#define VAL_GPIOK_PUPDR             (PIN_PUPDR_PULLUP(GPIOK_PIN0) |         \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN1) |         \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN2) |         \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN3) |         \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN4) |         \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN5) |         \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN6) |         \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN7) |         \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN8) |         \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN9) |         \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN10) |        \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN11) |        \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN12) |        \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN13) |        \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN14) |        \
                                     PIN_PUPDR_PULLUP(GPIOK_PIN15))
#define VAL_GPIOK_ODR               (PIN_ODR_HIGH(GPIOK_PIN0) |             \
                                     PIN_ODR_HIGH(GPIOK_PIN1) |             \
                                     PIN_ODR_HIGH(GPIOK_PIN2) |             \
                                     PIN_ODR_HIGH(GPIOK_PIN3) |             \
                                     PIN_ODR_HIGH(GPIOK_PIN4) |             \
                                     PIN_ODR_HIGH(GPIOK_PIN5) |             \
                                     PIN_ODR_HIGH(GPIOK_PIN6) |             \
                                     PIN_ODR_HIGH(GPIOK_PIN7) |             \
                                     PIN_ODR_HIGH(GPIOK_PIN8) |             \
                                     PIN_ODR_HIGH(GPIOK_PIN9) |             \
                                     PIN_ODR_HIGH(GPIOK_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOK_PIN11) |            \
                                     PIN_ODR_HIGH(GPIOK_PIN12) |            \
                                     PIN_ODR_HIGH(GPIOK_PIN13) |            \
                                     PIN_ODR_HIGH(GPIOK_PIN14) |            \
                                     PIN_ODR_HIGH(GPIOK_PIN15))
#define VAL_GPIOK_AFRL              (PIN_AFIO_AF(GPIOK_PIN0, 0U) |          \
                                     PIN_AFIO_AF(GPIOK_PIN1, 0U) |          \
                                     PIN_AFIO_AF(GPIOK_PIN2, 0U) |          \
                                     PIN_AFIO_AF(GPIOK_PIN3, 0U) |          \
                                     PIN_AFIO_AF(GPIOK_PIN4, 0U) |          \
                                     PIN_AFIO_AF(GPIOK_PIN5, 0U) |          \
                                     PIN_AFIO_AF(GPIOK_PIN6, 0U) |          \
                                     PIN_AFIO_AF(GPIOK_PIN7, 0U))
#define VAL_GPIOK_AFRH              (PIN_AFIO_AF(GPIOK_PIN8, 0U) |          \
                                     PIN_AFIO_AF(GPIOK_PIN9, 0U) |          \
                                     PIN_AFIO_AF(GPIOK_PIN10, 0U) |         \
                                     PIN_AFIO_AF(GPIOK_PIN11, 0U) |         \
                                     PIN_AFIO_AF(GPIOK_PIN12, 0U) |         \
                                     PIN_AFIO_AF(GPIOK_PIN13, 0U) |         \
                                     PIN_AFIO_AF(GPIOK_PIN14, 0U) |         \
                                     PIN_AFIO_AF(GPIOK_PIN15, 0U))

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
  void boardInit(void);
#ifdef __cplusplus
}
#endif
#endif /* _FROM_ASM_ */

#endif /* BOARD_H */
