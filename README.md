# Embebidos_Master_2018II

Descripción del curso realizado: 

En el trabajo realizado en el semestre 2018-2S  tuvo como objetivo principal la elaboración de
un  prototipo comercializable, a partir de 3 componentes (ONION – procesador (STM32F446ZET6) – FPGA(ICE40HX1K-TQ144).
Los elementos seleccionados tenía la finalidad de proveer al sistema un modulo con Linux embebido,
un microcontrolador donde se pudiese correr un sistema operativo de tiempo real, en éste caso ChibiOS y una
FPGA. 

En el proceso de diseño se realizaron diferentes pasos:

1. El primero de ellos fue la búsqueda de modelos que funcionaran con componentes hardware similares 
a los propuestos previamente, para lo cual se encontró  el Ejemplo de dsp_ice, que utiliza el STM32 y la FPGA
Documentación/Diseños_referencia/dsp_ice


era desarrollar un SDR(Software Defined Radio, sistema de radiocomunicaciones donde componentes típicamente 
implementados en hardware(mezcladores, filtros, moduladores/demoduladores, detectores, etc) son implementados 
en software. En el proyecto se utiliza un bus da datos de gestión y control, FSMC entre el STM32 y la FPGA, que
permite que oermite al STM32 escribir en la FPGA


2. Se realizó la lectura de las hojas de datos de los elementos base del diseño 

-ONION (Omega 2)
-STM32 (STM32F446ZET6)
-FPGA (ICE40HX1K-TQ144)

**Las Hojas de Datos se encuentran en la Carpeta /Documentación

3. De dicha lectura y de la referencia vista 
se estableció la comunicación entre los módulos principales del diseño y se decidió incluir una memoria flash para 
guardar allí los programas y realizar la lectura de los mimos desde la FPGA.

4. El proceso de diseño de la board a realizar, empezó desde el aprendizaje de Kicad, uso de Hojas jerárquicas, selección
de componentes y sus respectivos footprints hasta routing de toda la tarjeta considerando que el ancho de las vías
y caminos lo determina la cantidad de corriente que va a circular por los mismos

5. Durante los meses de diseño, de compra de materiales y soldadura de los mismos, se tuvieron inconvenientes tales como:
-Errores de Diseño en vías de alimentación
-Errores en la compra de los materiales(footprint erroneo, componentes del diseño que al momento de la compra estaban agotados)
-Errores de  diseño por falta de documentación (lectura imprecisa de los datasheets)
-Mala ubicación de los componentes 

6. Alcace:
El proyecto llegó hasta la programación del STM32 y la Omega 2, realizando los siguientes pasos:
1. Actualización del firmware de la Onion
2. Crosscompilación de paquetes en la Omega 2
3.Intalación de OpenOCD
4.Instalación de ChibiOS en el Computador
5. Editar board.h 
6. Editar el mcuconf.h
7. make 
8. Enviar el archivo .elf a la Omega2
scp ch.elf root@192.168.3.1:/root/





