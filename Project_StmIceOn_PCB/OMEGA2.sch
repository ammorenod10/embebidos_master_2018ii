EESchema Schematic File Version 4
LIBS:schematicProject-cache
EELAYER 26 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 5 5
Title "Poryecto Embebidos - Ana Moreno"
Date ""
Rev ""
Comp "Universidad Nacional de Colombia-Sede Bogotá"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4700 2050 4650 2050
Wire Wire Line
	4650 2050 4650 1850
Wire Wire Line
	4550 1850 4650 1850
Wire Wire Line
	6200 1850 6200 2050
Wire Wire Line
	4550 1850 4550 1900
$Comp
L schematicProject-rescue:OMEGA_2-omega-dock-new U?
U 1 1 5BA21305
P 5450 3100
AR Path="/5B7D7C2A/5BA21305" Ref="U?"  Part="1" 
AR Path="/5BA21305" Ref="U?"  Part="1" 
AR Path="/5BA1DFB2/5BA21305" Ref="U5"  Part="1" 
F 0 "U5" H 5800 4400 60  0000 C CNN
F 1 "OMEGA_2" H 5250 4400 60  0000 C CNN
F 2 "OMEGA2:XCVR_OMEGA2" H 5450 3700 60  0001 C CNN
F 3 "" H 5450 3700 60  0001 C CNN
	1    5450 3100
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:LED-device D?
U 1 1 5BA2130C
P 6900 3850
AR Path="/5BA2130C" Ref="D?"  Part="1" 
AR Path="/5BA1DFB2/5BA2130C" Ref="D2"  Part="1" 
F 0 "D2" H 6891 4066 50  0000 C CNN
F 1 "LED" H 6891 3975 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 6900 3850 50  0001 C CNN
F 3 "~" H 6900 3850 50  0001 C CNN
	1    6900 3850
	-1   0    0    1   
$EndComp
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5BA21313
P 7300 3850
AR Path="/5BA21313" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5BA21313" Ref="R3"  Part="1" 
F 0 "R3" V 7093 3850 50  0000 C CNN
F 1 "1K" V 7184 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7230 3850 50  0001 C CNN
F 3 "" H 7300 3850 50  0001 C CNN
	1    7300 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7050 3850 7150 3850
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR?
U 1 1 5BA2131C
P 7550 3850
AR Path="/5BA2131C" Ref="#PWR?"  Part="1" 
AR Path="/5BA1DFB2/5BA2131C" Ref="#PWR0120"  Part="1" 
F 0 "#PWR0120" H 7550 3600 50  0001 C CNN
F 1 "GND" V 7555 3722 50  0000 R CNN
F 2 "" H 7550 3850 50  0001 C CNN
F 3 "" H 7550 3850 50  0001 C CNN
	1    7550 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7550 3850 7450 3850
Wire Wire Line
	4700 2950 4400 2950
Wire Wire Line
	6200 3100 6500 3100
Text Label 6500 3100 2    50   ~ 0
Vout
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR?
U 1 1 5BA2133B
P 4550 1900
AR Path="/5BA2133B" Ref="#PWR?"  Part="1" 
AR Path="/5BA1DFB2/5BA2133B" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 4550 1650 50  0001 C CNN
F 1 "GND" H 4555 1727 50  0000 C CNN
F 2 "" H 4550 1900 50  0001 C CNN
F 3 "" H 4550 1900 50  0001 C CNN
	1    4550 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2500 6500 2500
Text HLabel 6500 2350 2    50   Input ~ 0
D+
Text HLabel 6500 2500 2    50   Input ~ 0
D-
Text Label 2450 5200 2    50   ~ 0
RESET
$Comp
L Switch:SW_Push SW?
U 1 1 5BA27EE8
P 1500 5000
AR Path="/5BA27EE8" Ref="SW?"  Part="1" 
AR Path="/5BA1DFB2/5BA27EE8" Ref="SW1"  Part="1" 
F 0 "SW1" H 1500 5285 50  0000 C CNN
F 1 "SW_Push" H 1500 5194 50  0000 C CNN
F 2 "kicad-footprints:SW_PUSH_6mm" H 1500 5200 50  0001 C CNN
F 3 "" H 1500 5200 50  0001 C CNN
	1    1500 5000
	0    -1   1    0   
$EndComp
Wire Wire Line
	2100 5600 2100 5500
Text HLabel 6200 2650 2    50   Input ~ 0
ON_RX0
Text HLabel 6200 2800 2    50   Input ~ 0
ON_TX0
Text HLabel 3500 3400 0    50   Input ~ 0
ON_MISO
Text HLabel 1700 3550 0    50   Output ~ 0
ON_MOSI
Text HLabel 3450 3700 0    50   Output ~ 0
ON_CLK
Wire Wire Line
	4700 2200 4400 2200
Wire Wire Line
	4700 2350 4400 2350
Wire Wire Line
	4700 2500 4400 2500
Wire Wire Line
	4700 2650 4400 2650
Text HLabel 4400 2200 0    50   Output ~ 0
JTMS
Text HLabel 4400 2350 0    50   Output ~ 0
JTCK
Text HLabel 4400 2500 0    50   Output ~ 0
JTDI
Text HLabel 4400 2650 0    50   Input ~ 0
JTDO
Wire Wire Line
	4700 2800 4350 2800
Text HLabel 4350 2800 0    50   Output ~ 0
JTRST
Text HLabel 6300 4000 2    50   Output ~ 0
BOOT0
Wire Wire Line
	6500 2350 6200 2350
Text HLabel 4400 2950 0    50   Output ~ 0
JNRST
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR?
U 1 1 5B98DB10
P 6350 1900
AR Path="/5B98DB10" Ref="#PWR?"  Part="1" 
AR Path="/5BA1DFB2/5B98DB10" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 6350 1650 50  0001 C CNN
F 1 "GND" H 6355 1727 50  0000 C CNN
F 2 "" H 6350 1900 50  0001 C CNN
F 3 "" H 6350 1900 50  0001 C CNN
	1    6350 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 1850 6350 1850
Wire Wire Line
	6350 1850 6350 1900
Text HLabel 2100 4700 2    50   Input ~ 0
ON_3V3
Text HLabel 3600 4000 0    50   Output ~ 0
ON_ICE_RST
Text HLabel 3400 3850 0    50   Output ~ 0
ON_CS
NoConn ~ 6200 3700
NoConn ~ 6200 3550
NoConn ~ 6200 3400
NoConn ~ 6200 3250
Wire Wire Line
	15600 8450 8050 8450
Text HLabel 7300 2200 2    50   Input ~ 0
ON_3V3
Text Label 7050 2950 2    50   ~ 0
Factory_Restore_Reset
Wire Wire Line
	6200 2950 7050 2950
Wire Wire Line
	4700 4300 4400 4300
Text Label 4400 4300 0    50   ~ 0
RESET
$Comp
L chip_media-cache-2018-09-25-05-03-38:C C52
U 1 1 5C4485CF
P 2100 4950
F 0 "C52" H 1986 4904 50  0000 R CNN
F 1 "0.1uF" H 1986 4995 50  0000 R CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2138 4800 50  0001 C CNN
F 3 "" H 2100 4950 50  0000 C CNN
	1    2100 4950
	1    0    0    1   
$EndComp
$Comp
L chip_media-cache-2018-09-25-05-03-38:C C51
U 1 1 5C449311
P 7050 2350
F 0 "C51" H 7165 2396 50  0000 L CNN
F 1 "0.1uF" H 7165 2305 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7088 2200 50  0001 C CNN
F 3 "" H 7050 2350 50  0000 C CNN
	1    7050 2350
	1    0    0    -1  
$EndComp
$Comp
L chip_media-cache-2018-09-25-05-03-38:GND #PWR0107
U 1 1 5C44936A
P 7050 2500
F 0 "#PWR0107" H 7050 2250 50  0001 C CNN
F 1 "GND" H 7055 2327 50  0000 C CNN
F 2 "" H 7050 2500 50  0000 C CNN
F 3 "" H 7050 2500 50  0000 C CNN
	1    7050 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2200 7050 2200
Connection ~ 7050 2200
Wire Wire Line
	7050 2200 7300 2200
Text HLabel 6750 3100 2    50   Input ~ 0
ON_VOUT
$Comp
L chip_media-cache-2018-09-25-05-03-38:C C50
U 1 1 5C449EFB
P 6500 3250
F 0 "C50" H 6615 3296 50  0000 L CNN
F 1 "0.1uF" H 6615 3205 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6538 3100 50  0001 C CNN
F 3 "" H 6500 3250 50  0000 C CNN
	1    6500 3250
	1    0    0    -1  
$EndComp
$Comp
L chip_media-cache-2018-09-25-05-03-38:GND #PWR0108
U 1 1 5C449F02
P 6500 3400
F 0 "#PWR0108" H 6500 3150 50  0001 C CNN
F 1 "GND" H 6505 3227 50  0000 C CNN
F 2 "" H 6500 3400 50  0000 C CNN
F 3 "" H 6500 3400 50  0000 C CNN
	1    6500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3100 6750 3100
Connection ~ 6500 3100
$Comp
L chip_media-cache-2018-09-25-05-03-38:R R22
U 1 1 5C451762
P 4550 3400
F 0 "R22" V 4757 3400 50  0000 C CNN
F 1 "100" V 4666 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4665 3400 50  0001 C CNN
F 3 "" H 4550 3400 50  0000 C CNN
	1    4550 3400
	0    -1   -1   0   
$EndComp
$Comp
L chip_media-cache-2018-09-25-05-03-38:R R21
U 1 1 5C452DD0
P 2150 3550
F 0 "R21" V 2357 3550 50  0000 C CNN
F 1 "100" V 2266 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2080 3550 50  0001 C CNN
F 3 "" H 2150 3550 50  0000 C CNN
	1    2150 3550
	0    -1   -1   0   
$EndComp
$Comp
L chip_media-cache-2018-09-25-05-03-38:R R20
U 1 1 5C4538BE
P 4150 3700
F 0 "R20" V 4357 3700 50  0000 C CNN
F 1 "100" V 4266 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4265 3700 50  0001 C CNN
F 3 "" H 4150 3700 50  0000 C CNN
	1    4150 3700
	0    -1   -1   0   
$EndComp
$Comp
L chip_media-cache-2018-09-25-05-03-38:R R19
U 1 1 5C456FF4
P 3950 3850
F 0 "R19" V 4157 3850 50  0000 C CNN
F 1 "100" V 4066 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4065 3850 50  0001 C CNN
F 3 "" H 3950 3850 50  0000 C CNN
	1    3950 3850
	0    -1   -1   0   
$EndComp
$Comp
L chip_media-cache-2018-09-25-05-03-38:R R16
U 1 1 5C456FFB
P 3750 4000
F 0 "R16" V 3957 4000 50  0000 C CNN
F 1 "100" V 3866 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3680 4000 50  0001 C CNN
F 3 "" H 3750 4000 50  0000 C CNN
	1    3750 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4300 3700 4700 3700
Wire Wire Line
	4100 3850 4700 3850
Wire Wire Line
	3900 4000 4700 4000
Wire Wire Line
	3450 3700 4000 3700
Wire Wire Line
	3400 3850 3800 3850
Wire Wire Line
	1700 3550 2000 3550
Wire Wire Line
	3500 3400 4400 3400
Wire Wire Line
	6200 4000 6300 4000
Wire Wire Line
	6200 3850 6750 3850
NoConn ~ 4700 3250
NoConn ~ 4700 4150
$Comp
L schematicProject-rescue:LED-device D?
U 1 1 5C4927A4
P 4000 3100
AR Path="/5C4927A4" Ref="D?"  Part="1" 
AR Path="/5BA1DFB2/5C4927A4" Ref="D8"  Part="1" 
F 0 "D8" H 3991 3316 50  0000 C CNN
F 1 "LED" H 3991 3225 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 4000 3100 50  0001 C CNN
F 3 "~" H 4000 3100 50  0001 C CNN
	1    4000 3100
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5C4927AB
P 3600 3100
AR Path="/5C4927AB" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5C4927AB" Ref="R8"  Part="1" 
F 0 "R8" V 3393 3100 50  0000 C CNN
F 1 "1K" V 3484 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3530 3100 50  0001 C CNN
F 3 "" H 3600 3100 50  0001 C CNN
	1    3600 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 3100 3750 3100
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR?
U 1 1 5C4927B3
P 3350 3100
AR Path="/5C4927B3" Ref="#PWR?"  Part="1" 
AR Path="/5BA1DFB2/5C4927B3" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 3350 2850 50  0001 C CNN
F 1 "GND" V 3355 2972 50  0000 R CNN
F 2 "" H 3350 3100 50  0001 C CNN
F 3 "" H 3350 3100 50  0001 C CNN
	1    3350 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 3100 3450 3100
Wire Wire Line
	4700 3100 4150 3100
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5C49A482
P 1750 5200
AR Path="/5C49A482" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5C49A482" Ref="R7"  Part="1" 
F 0 "R7" H 1680 5154 50  0000 R CNN
F 1 "10k" H 1680 5245 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1680 5200 50  0001 C CNN
F 3 "" H 1750 5200 50  0001 C CNN
	1    1750 5200
	0    1    -1   0   
$EndComp
$Comp
L chip_media-cache-2018-09-25-05-03-38:GND #PWR0114
U 1 1 5C49EAA6
P 2100 5600
F 0 "#PWR0114" H 2100 5350 50  0001 C CNN
F 1 "GND" H 2105 5472 50  0000 R CNN
F 2 "" H 2100 5600 50  0000 C CNN
F 3 "" H 2100 5600 50  0000 C CNN
	1    2100 5600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2100 4800 2100 4700
Wire Wire Line
	2100 5100 2100 5200
Wire Wire Line
	2100 5200 2450 5200
Text Label 6600 4150 2    50   ~ 0
I2C_SCL
Text Label 6600 4300 2    50   ~ 0
I2C_SDA
Wire Wire Line
	6200 4150 6600 4150
Wire Wire Line
	6200 4300 6600 4300
$Comp
L chip_media-cache-2018-09-25-05-03-38:R R?
U 1 1 5C5BA704
P 2600 3700
F 0 "R?" H 2670 3746 50  0000 L CNN
F 1 "1k" H 2670 3655 50  0000 L CNN
F 2 "" V 2530 3700 50  0000 C CNN
F 3 "" H 2600 3700 50  0000 C CNN
	1    2600 3700
	1    0    0    -1  
$EndComp
$Comp
L chip_media-cache-2018-09-25-05-03-38:GND #PWR?
U 1 1 5C5BB8CB
P 2600 3850
F 0 "#PWR?" H 2600 3600 50  0001 C CNN
F 1 "GND" H 2605 3677 50  0000 C CNN
F 2 "" H 2600 3850 50  0000 C CNN
F 3 "" H 2600 3850 50  0000 C CNN
	1    2600 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3550 2600 3550
Connection ~ 2600 3550
Wire Wire Line
	2600 3550 4700 3550
Wire Wire Line
	1500 5200 1600 5200
Connection ~ 2100 5200
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5BA27EE1
P 2100 5350
AR Path="/5BA27EE1" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5BA27EE1" Ref="R2"  Part="1" 
F 0 "R2" H 2030 5304 50  0000 R CNN
F 1 "4.7k" H 2030 5395 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2030 5350 50  0001 C CNN
F 3 "" H 2100 5350 50  0001 C CNN
	1    2100 5350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 5200 2100 5200
Connection ~ 2100 4800
Wire Wire Line
	1500 4800 2100 4800
Text Label 4400 5300 2    50   ~ 0
RESET
$Comp
L Switch:SW_Push SW?
U 1 1 5C66542A
P 3450 5100
AR Path="/5C66542A" Ref="SW?"  Part="1" 
AR Path="/5BA1DFB2/5C66542A" Ref="SW?"  Part="1" 
F 0 "SW?" H 3450 5385 50  0000 C CNN
F 1 "SW_Push" H 3450 5294 50  0000 C CNN
F 2 "kicad-footprints:SW_PUSH_6mm" H 3450 5300 50  0001 C CNN
F 3 "" H 3450 5300 50  0001 C CNN
	1    3450 5100
	0    -1   1    0   
$EndComp
Wire Wire Line
	4050 5700 4050 5600
Text HLabel 4100 5700 2    50   Input ~ 0
ON_3V3
$Comp
L chip_media-cache-2018-09-25-05-03-38:C C?
U 1 1 5C665433
P 4050 5050
F 0 "C?" H 3936 5004 50  0000 R CNN
F 1 "0.1uF" H 3936 5095 50  0000 R CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4088 4900 50  0001 C CNN
F 3 "" H 4050 5050 50  0000 C CNN
	1    4050 5050
	1    0    0    1   
$EndComp
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5C66543A
P 3000 6050
AR Path="/5C66543A" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5C66543A" Ref="R?"  Part="1" 
F 0 "R?" H 2930 6004 50  0000 R CNN
F 1 "10k" H 2930 6095 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2930 6050 50  0001 C CNN
F 3 "" H 3000 6050 50  0001 C CNN
	1    3000 6050
	0    1    -1   0   
$EndComp
$Comp
L chip_media-cache-2018-09-25-05-03-38:GND #PWR?
U 1 1 5C665441
P 4250 4750
F 0 "#PWR?" H 4250 4500 50  0001 C CNN
F 1 "GND" H 4255 4622 50  0000 R CNN
F 2 "" H 4250 4750 50  0000 C CNN
F 3 "" H 4250 4750 50  0000 C CNN
	1    4250 4750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4050 5200 4050 5300
Wire Wire Line
	4050 5300 4400 5300
Wire Wire Line
	3450 5300 3550 5300
Connection ~ 4050 5300
$Comp
L schematicProject-rescue:R-device R?
U 1 1 5C66544C
P 4050 5450
AR Path="/5C66544C" Ref="R?"  Part="1" 
AR Path="/5BA1DFB2/5C66544C" Ref="R?"  Part="1" 
F 0 "R?" H 3980 5404 50  0000 R CNN
F 1 "4.7k" H 3980 5495 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3980 5450 50  0001 C CNN
F 3 "" H 4050 5450 50  0001 C CNN
	1    4050 5450
	-1   0    0    -1  
$EndComp
Connection ~ 4050 4900
Wire Wire Line
	3450 4900 4050 4900
Wire Wire Line
	3550 5350 3550 5300
Wire Wire Line
	3550 5300 4050 5300
Connection ~ 3550 5300
Wire Wire Line
	4250 4750 4050 4750
Wire Wire Line
	4050 4750 4050 4900
Wire Wire Line
	4050 5700 4100 5700
$EndSCHEMATC
