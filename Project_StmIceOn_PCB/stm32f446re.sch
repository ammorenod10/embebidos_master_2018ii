EESchema Schematic File Version 4
LIBS:schematicProject-cache
EELAYER 26 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 5 5
Title "Poryecto Embebidos - Ana Moreno"
Date ""
Rev ""
Comp "Universidad Nacional de Colombia-Sede Bogotá"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3650 9700 0    50   Output ~ 0
FMC_NBL1
Text HLabel 3650 9600 0    50   Output ~ 0
FMC_NBL0
Text Label 3750 10300 0    50   ~ 0
FMC_D4
Text Label 3750 10400 0    50   ~ 0
FMC_D5
Text Label 3750 10500 0    50   ~ 0
FMC_D6
Text Label 3750 10600 0    50   ~ 0
FMC_D7
Text Label 3750 10700 0    50   ~ 0
FMC_D8
Text Label 3750 10800 0    50   ~ 0
FMC_D9
Text Label 3750 10900 0    50   ~ 0
FMC_D10
Text Label 3750 11000 0    50   ~ 0
FMC_D11
Text Label 3750 11100 0    50   ~ 0
FMC_D12
Text Label 13850 10900 2    50   ~ 0
FMC_A18
Text Label 13850 10800 2    50   ~ 0
FMC_A17
Text Label 13850 10700 2    50   ~ 0
FMC_A16
Text Label 3700 9800 0    50   ~ 0
FMC_A23
Text Label 3700 9900 0    50   ~ 0
FMC_A19
Text Label 3700 10000 0    50   ~ 0
FMC_A20
Text Label 3700 10200 0    50   ~ 0
FMC_A22
Text Label 3700 10100 0    50   ~ 0
FMC_A21
Wire Wire Line
	13500 9900 14150 9900
Text HLabel 14150 10000 2    60   Output ~ 0
FMC_NOE
Wire Wire Line
	13500 10000 14150 10000
Text HLabel 14150 10200 2    60   Output ~ 0
FMC_NWAIT
Wire Wire Line
	13500 10200 14150 10200
Text HLabel 14150 10300 2    60   Output ~ 0
FMC_NE1
Wire Wire Line
	13500 10100 14150 10100
Text HLabel 14150 10100 2    60   Output ~ 0
FMC_NWE
Wire Wire Line
	13500 6900 14150 6900
Text HLabel 14150 6900 2    60   Output ~ 0
FMC_NADV
$Comp
L schematicProject-rescue:GND-power #PWR0132
U 1 1 5B878C9A
P 16200 9300
F 0 "#PWR0132" H 16200 9050 50  0001 C CNN
F 1 "GND" H 16205 9127 50  0000 C CNN
F 2 "" H 16200 9300 50  0001 C CNN
F 3 "" H 16200 9300 50  0001 C CNN
	1    16200 9300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3650 9700 4100 9700
Wire Wire Line
	3650 9600 4100 9600
Wire Wire Line
	9200 3900 9200 3750
Wire Wire Line
	9000 3750 9000 3900
Wire Wire Line
	9100 3900 9100 3750
Wire Wire Line
	9000 3750 9100 3750
Connection ~ 9100 3750
Wire Wire Line
	9100 3750 9200 3750
Wire Wire Line
	8200 3900 8200 3750
Connection ~ 9000 3750
Wire Wire Line
	8300 3900 8300 3750
Wire Wire Line
	8200 3750 8300 3750
Connection ~ 8300 3750
Wire Wire Line
	8300 3750 8400 3750
Wire Wire Line
	8400 3900 8400 3750
Connection ~ 8400 3750
Wire Wire Line
	8500 3900 8500 3750
Wire Wire Line
	8400 3750 8500 3750
Connection ~ 8500 3750
Wire Wire Line
	8500 3750 8600 3750
Wire Wire Line
	8600 3900 8600 3750
Connection ~ 8600 3750
Wire Wire Line
	8600 3750 8700 3750
Wire Wire Line
	8700 3900 8700 3750
Connection ~ 8700 3750
Wire Wire Line
	8700 3750 8800 3750
Wire Wire Line
	8800 3900 8800 3750
Connection ~ 8800 3750
Wire Wire Line
	8800 3750 8900 3750
Wire Wire Line
	8900 3900 8900 3750
Connection ~ 8900 3750
Wire Wire Line
	8900 3750 9000 3750
Wire Wire Line
	8700 3550 8700 3750
$Comp
L schematicProject-rescue:C-device C29
U 1 1 5B8A98C9
P 16000 9000
F 0 "C29" H 15850 8900 50  0000 C CNN
F 1 "7pF" H 15800 9000 50  0000 C CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 16038 8850 50  0001 C CNN
F 3 "" H 16000 9000 50  0001 C CNN
	1    16000 9000
	0    1    1    0   
$EndComp
$Comp
L schematicProject-rescue:C-device C30
U 1 1 5B8E7F7A
P 16000 9600
F 0 "C30" H 15800 9700 50  0000 C CNN
F 1 "7pF" H 15800 9600 50  0000 C CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 16038 9450 50  0001 C CNN
F 3 "" H 16000 9600 50  0001 C CNN
	1    16000 9600
	0    -1   -1   0   
$EndComp
Connection ~ 16200 9300
Wire Wire Line
	13500 9300 14950 9300
Wire Wire Line
	13500 9400 14950 9400
Wire Wire Line
	13500 6000 14150 6000
Text HLabel 14150 6000 2    60   Input ~ 0
JTDI
Wire Wire Line
	13500 5800 14150 5800
Text HLabel 14150 5800 2    60   Input ~ 0
JTMS
Wire Wire Line
	13500 5900 14150 5900
Text HLabel 14150 5900 2    60   Input ~ 0
JTCK
Wire Wire Line
	13500 6600 14150 6600
Text HLabel 14150 6600 2    60   Input ~ 0
JTRST
Wire Wire Line
	4100 4500 3450 4500
Text HLabel 3450 4500 0    60   Input ~ 0
JNRST
Connection ~ 9200 3750
$Comp
L schematicProject-rescue:C-device C21
U 1 1 5B894013
P 3250 2950
F 0 "C21" H 3365 2996 50  0000 L CNN
F 1 "100nF" H 3365 2905 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3288 2800 50  0001 C CNN
F 3 "" H 3250 2950 50  0001 C CNN
	1    3250 2950
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C23
U 1 1 5B894088
P 3750 2950
F 0 "C23" H 3865 2996 50  0000 L CNN
F 1 "1uF" H 3865 2905 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3788 2800 50  0001 C CNN
F 3 "" H 3750 2950 50  0001 C CNN
	1    3750 2950
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0134
U 1 1 5B896EA5
P 3500 3200
F 0 "#PWR0134" H 3500 2950 50  0001 C CNN
F 1 "GND" H 3505 3027 50  0000 C CNN
F 2 "" H 3500 3200 50  0001 C CNN
F 3 "" H 3500 3200 50  0001 C CNN
	1    3500 3200
	1    0    0    -1  
$EndComp
Connection ~ 3500 3200
Wire Wire Line
	8300 11600 8300 11700
Wire Wire Line
	8400 11600 8400 11700
Wire Wire Line
	8300 11700 8400 11700
Connection ~ 8400 11700
Wire Wire Line
	8400 11700 8500 11700
Wire Wire Line
	8500 11600 8500 11700
Connection ~ 8500 11700
Wire Wire Line
	8500 11700 8600 11700
Wire Wire Line
	8600 11600 8600 11700
Connection ~ 8600 11700
Wire Wire Line
	8600 11700 8700 11700
Wire Wire Line
	8700 11600 8700 11700
Connection ~ 8700 11700
Wire Wire Line
	8700 11700 8800 11700
Wire Wire Line
	8800 11600 8800 11700
Connection ~ 8800 11700
Wire Wire Line
	8800 11700 8900 11700
Wire Wire Line
	8900 11600 8900 11700
Connection ~ 8900 11700
Wire Wire Line
	8900 11700 9000 11700
Wire Wire Line
	9000 11600 9000 11700
Connection ~ 9000 11700
Wire Wire Line
	9000 11700 9100 11700
Wire Wire Line
	9100 11600 9100 11700
$Comp
L schematicProject-rescue:GND-power #PWR0135
U 1 1 5B8C4B67
P 8800 11700
F 0 "#PWR0135" H 8800 11450 50  0001 C CNN
F 1 "GND" H 8805 11527 50  0000 C CNN
F 2 "" H 8800 11700 50  0001 C CNN
F 3 "" H 8800 11700 50  0001 C CNN
	1    8800 11700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 10300 4100 10300
Wire Wire Line
	3750 10400 4100 10400
Wire Wire Line
	3750 10500 4100 10500
Wire Wire Line
	3750 10600 4100 10600
Wire Wire Line
	3750 10700 4100 10700
Wire Wire Line
	3750 10800 4100 10800
Wire Wire Line
	3750 10900 4100 10900
Wire Wire Line
	3750 11000 4100 11000
Wire Wire Line
	3750 11100 4100 11100
Wire Wire Line
	13500 11100 13850 11100
Wire Wire Line
	13500 11000 13850 11000
Wire Wire Line
	13500 9600 13850 9600
Wire Wire Line
	13500 9700 13850 9700
Text Label 13850 9600 2    50   ~ 0
FMC_D2
Text Label 13850 9700 2    50   ~ 0
FMC_D3
Text Label 13850 11000 2    50   ~ 0
FMC_D0
Text Label 13850 11100 2    50   ~ 0
FMC_D1
Wire Wire Line
	13500 10400 13850 10400
Wire Wire Line
	13500 10500 13850 10500
Wire Wire Line
	13500 10600 13850 10600
Text Label 13850 10400 2    50   ~ 0
FMC_D13
Text Label 13850 10500 2    50   ~ 0
FMC_D14
Text Label 13850 10600 2    50   ~ 0
FMC_D15
$Comp
L schematicProject-rescue:C-device C18
U 1 1 5B8E02D7
P 4800 12450
F 0 "C18" H 4915 12496 50  0000 L CNN
F 1 "0.1uF" H 4915 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4838 12300 50  0001 C CNN
F 3 "" H 4800 12450 50  0001 C CNN
	1    4800 12450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C19
U 1 1 5B8E0431
P 5200 12450
F 0 "C19" H 5315 12496 50  0000 L CNN
F 1 "0.1uF" H 5315 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5238 12300 50  0001 C CNN
F 3 "" H 5200 12450 50  0001 C CNN
	1    5200 12450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C20
U 1 1 5B8E0482
P 5600 12450
F 0 "C20" H 5715 12496 50  0000 L CNN
F 1 "0.1uF" H 5715 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5638 12300 50  0001 C CNN
F 3 "" H 5600 12450 50  0001 C CNN
	1    5600 12450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C22
U 1 1 5B8E04DC
P 6050 12450
F 0 "C22" H 6165 12496 50  0000 L CNN
F 1 "0.1uF" H 6165 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6088 12300 50  0001 C CNN
F 3 "" H 6050 12450 50  0001 C CNN
	1    6050 12450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C24
U 1 1 5B8E0510
P 6450 12450
F 0 "C24" H 6565 12496 50  0000 L CNN
F 1 "0.1uF" H 6565 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6488 12300 50  0001 C CNN
F 3 "" H 6450 12450 50  0001 C CNN
	1    6450 12450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C17
U 1 1 5B8E0544
P 4400 12450
F 0 "C17" H 4515 12496 50  0000 L CNN
F 1 "0.1uF" H 4515 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4438 12300 50  0001 C CNN
F 3 "" H 4400 12450 50  0001 C CNN
	1    4400 12450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C16
U 1 1 5B8E0595
P 4000 12450
F 0 "C16" H 4115 12496 50  0000 L CNN
F 1 "0.1uF" H 4115 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4038 12300 50  0001 C CNN
F 3 "" H 4000 12450 50  0001 C CNN
	1    4000 12450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C25
U 1 1 5B8E0777
P 6900 12450
F 0 "C25" H 7015 12496 50  0000 L CNN
F 1 "0.1uF" H 7015 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6938 12300 50  0001 C CNN
F 3 "" H 6900 12450 50  0001 C CNN
	1    6900 12450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C26
U 1 1 5B91323A
P 7350 12450
F 0 "C26" H 7465 12496 50  0000 L CNN
F 1 "0.1uF" H 7465 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7388 12300 50  0001 C CNN
F 3 "" H 7350 12450 50  0001 C CNN
	1    7350 12450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C14
U 1 1 5B9132AC
P 3600 12450
F 0 "C14" H 3715 12496 50  0000 L CNN
F 1 "0.1uF" H 3715 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3638 12300 50  0001 C CNN
F 3 "" H 3600 12450 50  0001 C CNN
	1    3600 12450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 12300 4000 12200
Wire Wire Line
	4000 12200 4400 12200
Wire Wire Line
	4400 12200 4400 12300
Wire Wire Line
	4400 12200 4800 12200
Wire Wire Line
	4800 12200 4800 12300
Connection ~ 4400 12200
Wire Wire Line
	5200 12200 5200 12300
Wire Wire Line
	4800 12200 5200 12200
Wire Wire Line
	5600 12300 5600 12200
Connection ~ 5200 12200
Wire Wire Line
	6050 12300 6050 12200
Wire Wire Line
	5200 12200 5600 12200
Wire Wire Line
	6450 12300 6450 12200
Wire Wire Line
	6450 12200 6050 12200
Wire Wire Line
	6900 12300 6900 12200
Wire Wire Line
	6900 12200 6450 12200
Connection ~ 6450 12200
Wire Wire Line
	7350 12300 7350 12200
Wire Wire Line
	7350 12600 7350 12700
Wire Wire Line
	6900 12700 6900 12600
Wire Wire Line
	6450 12600 6450 12700
Wire Wire Line
	6450 12700 6900 12700
Wire Wire Line
	6050 12600 6050 12700
Wire Wire Line
	6050 12700 6450 12700
Connection ~ 6450 12700
Wire Wire Line
	5200 12600 5200 12700
Connection ~ 6050 12700
Wire Wire Line
	5600 12600 5600 12700
Wire Wire Line
	4000 12600 4000 12700
Wire Wire Line
	4000 12700 4400 12700
Wire Wire Line
	4400 12700 4400 12600
Wire Wire Line
	4800 12600 4800 12700
Connection ~ 5200 12700
Wire Wire Line
	5200 12700 5600 12700
Wire Wire Line
	4400 12700 4800 12700
Connection ~ 4400 12700
Connection ~ 4800 12700
Wire Wire Line
	4800 12700 5200 12700
Wire Wire Line
	3600 12300 3600 12200
Wire Wire Line
	3600 12200 4000 12200
Wire Wire Line
	4000 12700 3600 12700
Wire Wire Line
	3600 12700 3600 12600
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR0136
U 1 1 5B97F26D
P 5600 12800
F 0 "#PWR0136" H 5600 12550 50  0001 C CNN
F 1 "GND" H 5605 12627 50  0000 C CNN
F 2 "" H 5600 12800 50  0001 C CNN
F 3 "" H 5600 12800 50  0001 C CNN
	1    5600 12800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 12800 5600 12700
Wire Wire Line
	5600 12100 5600 12200
Wire Wire Line
	3850 4800 4100 4800
Text Label 3850 4800 0    50   ~ 0
BOOT0
Wire Wire Line
	13750 6400 13500 6400
Text Label 13750 6400 2    50   ~ 0
BOOT1
$Comp
L schematicProject-rescue:C-device C27
U 1 1 5B8E9E00
P 7750 12450
F 0 "C27" H 7865 12496 50  0000 L CNN
F 1 "0.1uF" H 7865 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7788 12300 50  0001 C CNN
F 3 "" H 7750 12450 50  0001 C CNN
	1    7750 12450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C28
U 1 1 5B8E9E07
P 8150 12450
F 0 "C28" H 8265 12496 50  0000 L CNN
F 1 "4.7uF" H 8265 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8188 12300 50  0001 C CNN
F 3 "" H 8150 12450 50  0001 C CNN
	1    8150 12450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 12300 7750 12200
Wire Wire Line
	8150 12300 8150 12200
Wire Wire Line
	8150 12600 8150 12700
Wire Wire Line
	7750 12700 7750 12600
Connection ~ 6050 12200
Connection ~ 4800 12200
Wire Wire Line
	6900 12200 7350 12200
Connection ~ 6900 12200
Wire Wire Line
	7350 12200 7750 12200
Connection ~ 7350 12200
Wire Wire Line
	7750 12200 8150 12200
Connection ~ 7750 12200
Wire Wire Line
	7750 12700 8150 12700
Wire Wire Line
	7750 12700 7350 12700
Connection ~ 7750 12700
Connection ~ 7350 12700
Wire Wire Line
	6900 12700 7350 12700
Connection ~ 6900 12700
Connection ~ 5600 12200
Connection ~ 5600 12700
Connection ~ 4000 12700
Connection ~ 4000 12200
$Comp
L schematicProject-rescue:C-device C12
U 1 1 5B9B0AAB
P 3200 12450
F 0 "C12" H 3315 12496 50  0000 L CNN
F 1 "0.1uF" H 3315 12405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3238 12300 50  0001 C CNN
F 3 "" H 3200 12450 50  0001 C CNN
	1    3200 12450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 12300 3200 12200
Wire Wire Line
	3200 12200 3600 12200
Wire Wire Line
	3600 12700 3200 12700
Wire Wire Line
	3200 12700 3200 12600
Wire Wire Line
	5600 12700 6050 12700
Connection ~ 3600 12200
Connection ~ 3600 12700
Wire Wire Line
	9400 3750 9400 3900
Text HLabel 10400 12400 0    50   Input ~ 0
BOOT0
Text Label 10400 12400 0    50   ~ 0
BOOT0
$Comp
L schematicProject-rescue:R-device R12
U 1 1 5BABDD36
P 11900 12650
F 0 "R12" H 11970 12696 50  0000 L CNN
F 1 "1k" H 11970 12605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 11830 12650 50  0001 C CNN
F 3 "" H 11900 12650 50  0001 C CNN
	1    11900 12650
	1    0    0    1   
$EndComp
Wire Wire Line
	11900 12400 11550 12400
Wire Wire Line
	11900 12400 11900 12500
Text Label 11600 12400 0    50   ~ 0
BOOT1
Wire Wire Line
	13500 6500 14150 6500
Text HLabel 14150 6500 2    60   Output ~ 0
JTDO
Wire Wire Line
	13500 8900 13950 8900
Wire Wire Line
	13500 9000 13950 9000
Text HLabel 13950 8900 2    50   Input ~ 0
UART_TX
Text HLabel 13950 9000 2    50   Input ~ 0
UART_RX
Wire Wire Line
	4100 7900 3700 7900
Wire Wire Line
	4100 8000 3700 8000
Text Label 3700 7900 2    50   ~ 0
STI2C_SDA
Text Label 3700 8000 2    50   ~ 0
STI2C_SCL
Wire Wire Line
	13500 5200 14100 5200
Wire Wire Line
	13500 5100 14100 5100
Text Label 14100 5100 2    50   ~ 0
CM2B
Text Label 14100 5200 2    50   ~ 0
CM2D
$Comp
L schematicProject-rescue:Conn_01x03-Connector_Generic J13
U 1 1 5B9C583F
P 9250 2050
F 0 "J13" H 9200 1800 50  0000 L CNN
F 1 "SEN5" H 9150 2250 43  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 9250 2050 50  0001 C CNN
F 3 "~" H 9250 2050 50  0001 C CNN
	1    9250 2050
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:Conn_01x03-Connector_Generic J14
U 1 1 5B9C5CE8
P 10050 2050
F 0 "J14" H 10000 1800 50  0000 L CNN
F 1 "SEN6" H 9950 2250 43  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 10050 2050 50  0001 C CNN
F 3 "~" H 10050 2050 50  0001 C CNN
	1    10050 2050
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:Conn_01x03-Connector_Generic J15
U 1 1 5B9D21FC
P 10700 2050
F 0 "J15" H 10650 1800 50  0000 L CNN
F 1 "SEN7" H 10600 2250 43  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 10700 2050 50  0001 C CNN
F 3 "~" H 10700 2050 50  0001 C CNN
	1    10700 2050
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:Conn_01x03-Connector_Generic J9
U 1 1 5BA1D9D4
P 6400 2050
F 0 "J9" H 6350 1800 50  0000 L CNN
F 1 "SEN1" H 6300 2250 43  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 6400 2050 50  0001 C CNN
F 3 "~" H 6400 2050 50  0001 C CNN
	1    6400 2050
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:Conn_01x03-Connector_Generic J10
U 1 1 5BA1D9DB
P 7200 2050
F 0 "J10" H 7150 1800 50  0000 L CNN
F 1 "SEN2" H 7100 2250 43  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 7200 2050 50  0001 C CNN
F 3 "~" H 7200 2050 50  0001 C CNN
	1    7200 2050
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:Conn_01x03-Connector_Generic J11
U 1 1 5BA1D9E2
P 7850 2050
F 0 "J11" H 7800 1800 50  0000 L CNN
F 1 "SEN3" H 7750 2250 43  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 7850 2050 50  0001 C CNN
F 3 "~" H 7850 2050 50  0001 C CNN
	1    7850 2050
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:Conn_01x03-Connector_Generic J12
U 1 1 5BA1D9E9
P 8500 2050
F 0 "J12" H 8450 1800 50  0000 L CNN
F 1 "SEN4" H 8400 2250 43  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 8500 2050 50  0001 C CNN
F 3 "~" H 8500 2050 50  0001 C CNN
	1    8500 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 1950 6100 1950
Wire Wire Line
	6100 1950 6100 1550
Connection ~ 6100 1550
Wire Wire Line
	6100 1550 5700 1550
Wire Wire Line
	7000 1950 6900 1950
Wire Wire Line
	6900 1950 6900 1550
Connection ~ 6900 1550
Wire Wire Line
	7650 1950 7550 1950
Wire Wire Line
	7550 1950 7550 1550
Wire Wire Line
	6900 1550 7550 1550
Connection ~ 7550 1550
Wire Wire Line
	7550 1550 8250 1550
Wire Wire Line
	8300 1950 8250 1950
Wire Wire Line
	8250 1950 8250 1550
Connection ~ 8250 1550
Wire Wire Line
	9050 1950 8950 1950
Wire Wire Line
	8950 1950 8950 1550
Connection ~ 8950 1550
Wire Wire Line
	9850 1950 9800 1950
Wire Wire Line
	9800 1950 9800 1550
Connection ~ 9800 1550
Wire Wire Line
	9800 1550 10400 1550
Wire Wire Line
	10500 1950 10400 1950
Wire Wire Line
	10400 1950 10400 1550
$Comp
L schematicProject-rescue:GND-power #PWR0138
U 1 1 5BAAFD23
P 6200 2350
F 0 "#PWR0138" H 6200 2100 50  0001 C CNN
F 1 "GND" H 6205 2177 50  0000 C CNN
F 2 "" H 6200 2350 50  0001 C CNN
F 3 "" H 6200 2350 50  0001 C CNN
	1    6200 2350
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0139
U 1 1 5BAAFE02
P 7000 2350
F 0 "#PWR0139" H 7000 2100 50  0001 C CNN
F 1 "GND" H 7005 2177 50  0000 C CNN
F 2 "" H 7000 2350 50  0001 C CNN
F 3 "" H 7000 2350 50  0001 C CNN
	1    7000 2350
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0140
U 1 1 5BABE45B
P 7650 2350
F 0 "#PWR0140" H 7650 2100 50  0001 C CNN
F 1 "GND" H 7655 2177 50  0000 C CNN
F 2 "" H 7650 2350 50  0001 C CNN
F 3 "" H 7650 2350 50  0001 C CNN
	1    7650 2350
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0141
U 1 1 5BACCABA
P 8300 2350
F 0 "#PWR0141" H 8300 2100 50  0001 C CNN
F 1 "GND" H 8305 2177 50  0000 C CNN
F 2 "" H 8300 2350 50  0001 C CNN
F 3 "" H 8300 2350 50  0001 C CNN
	1    8300 2350
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0142
U 1 1 5BADB2D1
P 9050 2350
F 0 "#PWR0142" H 9050 2100 50  0001 C CNN
F 1 "GND" H 9055 2177 50  0000 C CNN
F 2 "" H 9050 2350 50  0001 C CNN
F 3 "" H 9050 2350 50  0001 C CNN
	1    9050 2350
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0143
U 1 1 5BAE99F8
P 9850 2350
F 0 "#PWR0143" H 9850 2100 50  0001 C CNN
F 1 "GND" H 9855 2177 50  0000 C CNN
F 2 "" H 9850 2350 50  0001 C CNN
F 3 "" H 9850 2350 50  0001 C CNN
	1    9850 2350
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0144
U 1 1 5BAF7FDF
P 10500 2350
F 0 "#PWR0144" H 10500 2100 50  0001 C CNN
F 1 "GND" H 10505 2177 50  0000 C CNN
F 2 "" H 10500 2350 50  0001 C CNN
F 3 "" H 10500 2350 50  0001 C CNN
	1    10500 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2150 6200 2350
Wire Wire Line
	7000 2150 7000 2350
Wire Wire Line
	7650 2150 7650 2350
Wire Wire Line
	8300 2150 8300 2350
Wire Wire Line
	9050 2150 9050 2350
Wire Wire Line
	9850 2150 9850 2350
Wire Wire Line
	10500 2150 10500 2350
Wire Wire Line
	10500 2050 10250 2050
Wire Wire Line
	9850 2050 9600 2050
Wire Wire Line
	9050 2050 8800 2050
Wire Wire Line
	8300 2050 8100 2050
Wire Wire Line
	7650 2050 7450 2050
Wire Wire Line
	7000 2050 6750 2050
Wire Wire Line
	5950 2050 6200 2050
Text Label 5950 2050 0    50   ~ 0
SEN1
Text Label 6750 2050 0    50   ~ 0
SEN2
Text Label 7450 2050 0    50   ~ 0
SEN3
Text Label 8100 2050 0    50   ~ 0
SEN4
Text Label 8800 2050 0    50   ~ 0
SEN5
Text Label 9600 2050 0    50   ~ 0
SEN6
Text Label 10250 2050 0    50   ~ 0
SEN7
$Comp
L schematicProject-rescue:R-device R10
U 1 1 5BB03935
P 10750 12150
F 0 "R10" H 10820 12196 50  0000 L CNN
F 1 "1k" H 10820 12105 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10680 12150 50  0001 C CNN
F 3 "" H 10750 12150 50  0001 C CNN
	1    10750 12150
	-1   0    0    1   
$EndComp
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR0145
U 1 1 5BB0393C
P 10750 12900
F 0 "#PWR0145" H 10750 12650 50  0001 C CNN
F 1 "GND" H 10755 12727 50  0000 C CNN
F 2 "" H 10750 12900 50  0001 C CNN
F 3 "" H 10750 12900 50  0001 C CNN
	1    10750 12900
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:R-device R11
U 1 1 5BB03943
P 10750 12650
F 0 "R11" H 10820 12696 50  0000 L CNN
F 1 "1k" H 10820 12605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10680 12650 50  0001 C CNN
F 3 "" H 10750 12650 50  0001 C CNN
	1    10750 12650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 12800 10750 12900
Text HLabel 11550 12400 0    50   3State ~ 0
BOOT1
Wire Wire Line
	10750 12300 10750 12400
Wire Wire Line
	10400 12400 10750 12400
Connection ~ 10750 12400
Wire Wire Line
	10750 12400 10750 12500
$Comp
L schematicProject-rescue:C-device C13
U 1 1 5BB73605
P 3400 5350
F 0 "C13" H 3515 5396 50  0000 L CNN
F 1 "2.2uF" H 3515 5305 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3438 5200 50  0001 C CNN
F 3 "" H 3400 5350 50  0001 C CNN
	1    3400 5350
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-device C15
U 1 1 5BB73689
P 3800 5450
F 0 "C15" H 3915 5496 50  0000 L CNN
F 1 "2.2uF" H 3915 5405 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3838 5300 50  0001 C CNN
F 3 "" H 3800 5450 50  0001 C CNN
	1    3800 5450
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0146
U 1 1 5BB737A2
P 3800 5600
F 0 "#PWR0146" H 3800 5350 50  0001 C CNN
F 1 "GND" H 3805 5427 50  0000 C CNN
F 2 "" H 3800 5600 50  0001 C CNN
F 3 "" H 3800 5600 50  0001 C CNN
	1    3800 5600
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0147
U 1 1 5BB73804
P 3400 5500
F 0 "#PWR0147" H 3400 5250 50  0001 C CNN
F 1 "GND" H 3405 5327 50  0000 C CNN
F 2 "" H 3400 5500 50  0001 C CNN
F 3 "" H 3400 5500 50  0001 C CNN
	1    3400 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 5200 4100 5200
Wire Wire Line
	3800 5300 4100 5300
Wire Wire Line
	9200 3750 9400 3750
Wire Wire Line
	9300 3900 9300 3600
Text Label 9300 3600 3    50   ~ 0
VDDA
$Comp
L schematicProject-rescue:L-device L2
U 1 1 5BBB414F
P 2950 2750
F 0 "L2" V 3140 2750 50  0000 C CNN
F 1 "L" V 3049 2750 50  0000 C CNN
F 2 "kicad-footprints:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2950 2750 50  0001 C CNN
F 3 "" H 2950 2750 50  0001 C CNN
	1    2950 2750
	0    -1   -1   0   
$EndComp
Text Label 3250 2750 0    50   ~ 0
VDDA
Wire Wire Line
	3750 2750 3750 2800
Wire Wire Line
	3100 2750 3250 2750
Wire Wire Line
	3250 2800 3250 2750
Connection ~ 3250 2750
Wire Wire Line
	3250 2750 3750 2750
Wire Wire Line
	3250 3100 3250 3200
Wire Wire Line
	3250 3200 3500 3200
Wire Wire Line
	3750 3100 3750 3200
Wire Wire Line
	3500 3200 3750 3200
$Comp
L schematicProject-rescue:L-device L3
U 1 1 5BC561DA
P 9350 11800
F 0 "L3" V 9540 11800 50  0000 C CNN
F 1 "L" V 9449 11800 50  0000 C CNN
F 2 "kicad-footprints:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9350 11800 50  0001 C CNN
F 3 "" H 9350 11800 50  0001 C CNN
	1    9350 11800
	0    -1   -1   0   
$EndComp
$Comp
L schematicProject-rescue:GND-power #PWR0148
U 1 1 5BC56356
P 9600 11800
F 0 "#PWR0148" H 9600 11550 50  0001 C CNN
F 1 "GND" V 9605 11672 50  0000 R CNN
F 2 "" H 9600 11800 50  0001 C CNN
F 3 "" H 9600 11800 50  0001 C CNN
	1    9600 11800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9500 11800 9600 11800
Wire Wire Line
	9200 11600 9200 11800
Wire Wire Line
	8950 1550 9800 1550
Wire Wire Line
	8250 1550 8950 1550
$Comp
L schematicProject-rescue:DRV8835-chip_media-cache-2018-09-26-12-25-26 U11
U 1 1 5BB007A1
P 18600 3500
F 0 "U11" H 18500 4187 60  0000 C CNN
F 1 "DRV8835" H 18500 4081 60  0000 C CNN
F 2 "housinngs:MSOP-12-1EP_3x4mm_Pitch0.65mm" H 18500 4081 60  0001 C CNN
F 3 "" H 18600 3500 60  0000 C CNN
	1    18600 3500
	-1   0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:ULN2003-chip_media-cache-2018-09-25-05-03-38 U9
U 1 1 5BB01A05
P 17950 5250
F 0 "U9" H 17950 5915 50  0000 C CNN
F 1 "ULN2003" H 17950 5824 50  0000 C CNN
F 2 "housinngs:TSSOP-16_4.4x5mm_Pitch0.65mm" H 17950 5823 50  0001 C CNN
F 3 "" H 17950 5250 50  0000 C CNN
	1    17950 5250
	1    0    0    -1  
$EndComp
Wire Notes Line
	15950 4100 21450 4100
Wire Notes Line
	15950 2400 21450 2400
Text Notes 16250 2600 0    91   ~ 0
DC MOTORS\n
Wire Wire Line
	18050 3100 17550 3100
Wire Wire Line
	17550 3100 17550 3050
Wire Wire Line
	18050 3200 17550 3200
Wire Wire Line
	17550 3200 17550 3100
Wire Wire Line
	20250 3200 19350 3200
Wire Wire Line
	20250 3300 19350 3300
Wire Wire Line
	20250 3500 19350 3500
Wire Wire Line
	20250 3400 19350 3400
Wire Wire Line
	19350 3600 19850 3600
Wire Wire Line
	19850 3600 19850 3650
Wire Wire Line
	19350 3650 19850 3650
Connection ~ 19850 3650
Wire Wire Line
	19850 3650 19850 3750
$Comp
L schematicProject-rescue:GND-chip_media-cache-2018-09-25-05-03-38 #PWR0149
U 1 1 5BCCEE9A
P 19850 3750
F 0 "#PWR0149" H 19850 3500 50  0001 C CNN
F 1 "GND" H 19855 3577 50  0000 C CNN
F 2 "" H 19850 3750 50  0000 C CNN
F 3 "" H 19850 3750 50  0000 C CNN
	1    19850 3750
	1    0    0    -1  
$EndComp
Text Label 20100 3100 0    67   ~ 0
VM
Text Label 19500 3200 0    71   ~ 0
MOB
Text Label 19500 3300 0    71   ~ 0
MOA
Text Label 19500 3400 0    71   ~ 0
M1B
Text Label 19500 3500 0    71   ~ 0
M1A
Text Label 17500 3500 0    71   ~ 0
DIR_M1
Text Label 17500 3600 0    71   ~ 0
SP_M1
Text Notes 16300 4400 0    91   ~ 0
STEP  MOTORS\n
Wire Notes Line
	15950 2400 15950 8100
Wire Notes Line
	15950 8100 21450 8100
Wire Notes Line
	21450 2400 21450 8100
$Comp
L schematicProject-rescue:CONN_01X04-chip_media-cache-2018-09-25-05-03-38 P3
U 1 1 5BD1CA69
P 20450 3350
F 0 "P3" H 20528 3391 50  0000 L CNN
F 1 "CONN_01X04" H 20528 3300 50  0000 L CNN
F 2 "kicad footprints:PinHeader_1x04_P2.54mm_Vertical" H 20450 3350 50  0001 C CNN
F 3 "" H 20450 3350 50  0000 C CNN
	1    20450 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	17300 4950 16500 4950
Wire Wire Line
	17300 5050 16500 5050
Wire Wire Line
	17300 5150 16500 5150
Wire Wire Line
	17300 5250 16500 5250
Text Label 16500 4950 0    71   ~ 0
CM1C
Text Label 16500 5050 0    71   ~ 0
CM1A
Text Label 16500 5150 0    71   ~ 0
CM1B
Text Label 16500 5250 0    71   ~ 0
CM1D
Wire Wire Line
	18600 5050 19150 5050
Wire Wire Line
	19150 5050 19150 5000
Wire Wire Line
	18600 4950 19400 4950
Wire Wire Line
	19400 4950 19400 5200
Wire Wire Line
	18600 5150 19550 5150
Wire Wire Line
	18600 5250 19550 5250
$Comp
L schematicProject-rescue:D_Zener_Small-chip_media-cache-2018-09-25-05-03-38 D5
U 1 1 5BF8708C
P 19000 5650
F 0 "D5" H 19000 5855 50  0000 C CNN
F 1 "D_Zener_Small" H 19000 5764 50  0000 C CNN
F 2 "kicad footprints:D_SOD-123F" V 19000 5650 50  0001 C CNN
F 3 "" V 19000 5650 50  0000 C CNN
	1    19000 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	18600 5650 18900 5650
Wire Wire Line
	19100 5650 19550 5650
Wire Wire Line
	19550 5650 19550 5400
Wire Wire Line
	19550 5250 19550 5300
Wire Wire Line
	19550 5150 19550 5100
Wire Wire Line
	19550 5300 19950 5300
Wire Wire Line
	19400 5200 19950 5200
Wire Wire Line
	19550 5100 19950 5100
Wire Wire Line
	19150 5000 19950 5000
$Comp
L schematicProject-rescue:CONN_01X05-chip_media-cache-2018-09-25-05-03-38 P1
U 1 1 5BF870A9
P 20150 5200
F 0 "P1" H 20228 5241 50  0000 L CNN
F 1 "CONN_01X05" H 20228 5150 50  0000 L CNN
F 2 "kicad footprints:PinHeader_1x05_P2.54mm_Vertical" H 20150 5200 50  0001 C CNN
F 3 "" H 20150 5200 50  0000 C CNN
	1    20150 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	19550 5400 19950 5400
Wire Wire Line
	13500 8500 14100 8500
Wire Wire Line
	13500 8400 14100 8400
Wire Wire Line
	13500 8600 14100 8600
Text Label 14100 8600 2    50   ~ 0
CM1C
Text Label 14100 8500 2    50   ~ 0
CM1A
Text Label 14100 8400 2    50   ~ 0
CM1B
Wire Wire Line
	13500 4900 14100 4900
Wire Wire Line
	13500 5000 14100 5000
Wire Wire Line
	13500 8300 14100 8300
Text Label 14100 8300 2    50   ~ 0
CM1D
Text Label 14100 4900 2    50   ~ 0
CM2C
Text Label 14100 5000 2    50   ~ 0
CM2A
Wire Wire Line
	13500 6300 14100 6300
Wire Wire Line
	13500 6200 14100 6200
Text Label 14100 6200 2    50   ~ 0
DIR_M1
Text Label 14100 6300 2    50   ~ 0
SP_M1
Wire Wire Line
	13500 7300 14100 7300
Wire Wire Line
	13500 7200 14100 7200
Text Label 14100 7200 2    50   ~ 0
DIR_M0
Text Label 14100 7300 2    50   ~ 0
SP_M0
$Comp
L schematicProject-rescue:LED-device D4
U 1 1 5BC1E27D
P 14150 9800
F 0 "D4" H 14142 9545 50  0000 C CNN
F 1 "LED" H 14142 9636 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 14150 9800 50  0001 C CNN
F 3 "~" H 14150 9800 50  0001 C CNN
	1    14150 9800
	-1   0    0    1   
$EndComp
$Comp
L schematicProject-rescue:R-device R9
U 1 1 5BC1E284
P 14500 9800
F 0 "R9" V 14293 9800 50  0000 C CNN
F 1 "1k" V 14384 9800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 14430 9800 50  0001 C CNN
F 3 "" H 14500 9800 50  0001 C CNN
	1    14500 9800
	0    1    1    0   
$EndComp
$Comp
L schematicProject-rescue:GND-omega-dock-new-cache #PWR0150
U 1 1 5BC1E28C
P 14750 9800
F 0 "#PWR0150" H 14750 9550 50  0001 C CNN
F 1 "GND" V 14755 9672 50  0000 R CNN
F 2 "" H 14750 9800 50  0001 C CNN
F 3 "" H 14750 9800 50  0001 C CNN
	1    14750 9800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	13500 9800 14000 9800
Wire Wire Line
	14300 9800 14350 9800
Wire Wire Line
	14650 9800 14750 9800
Wire Wire Line
	13500 8100 14100 8100
Text Label 14100 8100 2    50   ~ 0
SEN6
Wire Wire Line
	13500 4800 14100 4800
Text Label 14100 4800 2    50   ~ 0
SEN1
Wire Wire Line
	13500 4700 14100 4700
Text Label 14100 4700 2    50   ~ 0
SEN2
Wire Wire Line
	13500 4600 14100 4600
Text Label 14100 4600 2    50   ~ 0
SEN3
Wire Wire Line
	13500 4500 14100 4500
Text Label 14100 4500 2    50   ~ 0
SEN4
Wire Wire Line
	13500 8200 14100 8200
Text Label 14100 8200 2    50   ~ 0
SEN5
Wire Wire Line
	13500 8000 14100 8000
Text Label 14100 8000 2    50   ~ 0
SEN7
Wire Wire Line
	13500 7900 14100 7900
Text Label 14100 7900 2    50   ~ 0
SEN8
$Comp
L schematicProject-rescue:Conn_01x03-Connector_Generic J16
U 1 1 5BE86EC9
P 11300 2050
F 0 "J16" H 11250 1800 50  0000 L CNN
F 1 "SEN8" H 11200 2250 43  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 11300 2050 50  0001 C CNN
F 3 "~" H 11300 2050 50  0001 C CNN
	1    11300 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 1550 11000 1550
Wire Wire Line
	11100 1950 11000 1950
Wire Wire Line
	11000 1950 11000 1550
$Comp
L schematicProject-rescue:GND-power #PWR0151
U 1 1 5BE86ED3
P 11100 2350
F 0 "#PWR0151" H 11100 2100 50  0001 C CNN
F 1 "GND" H 11105 2177 50  0000 C CNN
F 2 "" H 11100 2350 50  0001 C CNN
F 3 "" H 11100 2350 50  0001 C CNN
	1    11100 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	11100 2150 11100 2350
Wire Wire Line
	11100 2050 10850 2050
Text Label 10850 2050 0    50   ~ 0
SEN8
Connection ~ 10400 1550
$Comp
L schematicProject-rescue:Battery_Cell-Device BT1
U 1 1 5BEC0C8E
P 2250 5250
F 0 "BT1" H 2368 5346 50  0000 L CNN
F 1 "Battery_Cell" H 2368 5255 50  0000 L CNN
F 2 "libraries_batteries:CR2032_HOLDER" V 2250 5310 50  0001 C CNN
F 3 "" V 2250 5310 50  0001 C CNN
	1    2250 5250
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-chip_media-cache-2018-09-25-05-03-38 #PWR0152
U 1 1 5BEC0DC7
P 2250 5350
F 0 "#PWR0152" H 2250 5100 50  0001 C CNN
F 1 "GND" H 2255 5177 50  0000 C CNN
F 2 "" H 2250 5350 50  0000 C CNN
F 3 "" H 2250 5350 50  0000 C CNN
	1    2250 5350
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:C-Device C11
U 1 1 5BEC0F5A
P 2950 5200
F 0 "C11" H 3065 5246 50  0000 L CNN
F 1 "0.1uF" H 3065 5155 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2988 5050 50  0001 C CNN
F 3 "" H 2950 5200 50  0000 C CNN
	1    2950 5200
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:GND-chip_media-cache-2018-09-25-05-03-38 #PWR0153
U 1 1 5BEC11E8
P 2950 5350
F 0 "#PWR0153" H 2950 5100 50  0001 C CNN
F 1 "GND" H 2955 5177 50  0000 C CNN
F 2 "" H 2950 5350 50  0000 C CNN
F 3 "" H 2950 5350 50  0000 C CNN
	1    2950 5350
	1    0    0    -1  
$EndComp
$Comp
L schematicProject-rescue:R-chip_media-cache-2018-09-25-05-03-38 R6
U 1 1 5BEC16BD
P 2600 5000
F 0 "R6" V 2393 5000 50  0000 C CNN
F 1 "0" V 2484 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2530 5000 50  0001 C CNN
F 3 "" H 2600 5000 50  0000 C CNN
	1    2600 5000
	0    1    1    0   
$EndComp
$Comp
L schematicProject-rescue:R-chip_media-cache-2018-09-25-05-03-38 R5
U 1 1 5BEC1970
P 2600 4650
F 0 "R5" V 2393 4650 50  0000 C CNN
F 1 "R" V 2484 4650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2530 4650 50  0001 C CNN
F 3 "" H 2600 4650 50  0000 C CNN
	1    2600 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 5050 2950 5000
Connection ~ 2950 5000
Wire Wire Line
	2950 5000 2750 5000
Wire Wire Line
	2450 5000 2250 5000
Wire Wire Line
	2250 5000 2250 5050
Wire Wire Line
	2950 5000 4100 5000
Wire Wire Line
	2250 4600 2250 4650
Wire Wire Line
	2250 4650 2450 4650
Wire Wire Line
	2750 4650 2950 4650
Wire Wire Line
	2950 4650 2950 5000
NoConn ~ 4100 6200
NoConn ~ 4100 6300
NoConn ~ 4100 6400
NoConn ~ 4100 7200
NoConn ~ 4100 7400
NoConn ~ 4100 7700
NoConn ~ 4100 9400
NoConn ~ 4100 9300
NoConn ~ 4100 9100
NoConn ~ 13500 9100
NoConn ~ 13500 9200
NoConn ~ 4100 6700
NoConn ~ 4100 6800
NoConn ~ 4100 6900
NoConn ~ 4100 7000
NoConn ~ 4100 7100
NoConn ~ 4100 7300
NoConn ~ 4100 7600
Wire Wire Line
	4100 8700 3500 8700
Text Label 3500 8700 0    50   ~ 0
ADC0
Wire Wire Line
	4100 8800 3500 8800
Text Label 3500 8800 0    50   ~ 0
ADC1
NoConn ~ 4100 9200
Wire Wire Line
	13500 5300 14100 5300
Text Label 14100 5300 2    50   ~ 0
GPIO4
Wire Wire Line
	13500 5500 14100 5500
Text Label 14100 5500 2    50   ~ 0
GPIO6
Wire Wire Line
	13500 5400 14100 5400
Text Label 14100 5400 2    50   ~ 0
GPIO5
Wire Wire Line
	13500 5600 14100 5600
Text Label 14100 5600 2    50   ~ 0
GPIO7
Text Label 14100 7600 2    50   ~ 0
GPIO0
Text Label 14100 7700 2    50   ~ 0
GPIO1
Text HLabel 20300 3100 2    67   Input ~ 0
VM
NoConn ~ 17300 4850
NoConn ~ 17300 5350
NoConn ~ 17300 5450
NoConn ~ 18600 4850
NoConn ~ 18600 5350
NoConn ~ 18600 5450
NoConn ~ 4100 6600
NoConn ~ 4100 6500
Text HLabel 5700 1550 0    50   Input ~ 0
STM3V3
Text HLabel 2250 4600 1    50   Input ~ 0
STM3V3
Text HLabel 5600 12100 1    50   Input ~ 0
STM3V3
Text HLabel 17250 3050 0    50   Input ~ 0
STM3V3
Wire Wire Line
	14950 9000 14950 9300
Wire Wire Line
	14950 9400 14950 9600
Wire Wire Line
	16150 9000 16200 9000
Wire Wire Line
	16200 9000 16200 9300
Wire Wire Line
	16200 9600 16150 9600
Wire Wire Line
	16200 9300 16200 9600
$Comp
L schematicProject-rescue:GND-power #PWR0133
U 1 1 5BDFA57A
P 1400 6000
F 0 "#PWR0133" H 1400 5750 50  0001 C CNN
F 1 "GND" H 1405 5827 50  0000 C CNN
F 2 "" H 1400 6000 50  0001 C CNN
F 3 "" H 1400 6000 50  0001 C CNN
	1    1400 6000
	0    1    1    0   
$EndComp
$Comp
L schematicProject-rescue:C-device C10
U 1 1 5BDFA580
P 1600 6300
F 0 "C10" H 1450 6200 50  0000 C CNN
F 1 "22pF" H 1400 6300 50  0000 C CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1638 6150 50  0001 C CNN
F 3 "" H 1600 6300 50  0001 C CNN
	1    1600 6300
	0    -1   -1   0   
$EndComp
$Comp
L schematicProject-rescue:C-device C9
U 1 1 5BDFA587
P 1600 5700
F 0 "C9" H 1400 5800 50  0000 C CNN
F 1 "22pF" H 1400 5700 50  0000 C CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1638 5550 50  0001 C CNN
F 3 "" H 1600 5700 50  0001 C CNN
	1    1600 5700
	0    1    1    0   
$EndComp
Connection ~ 1400 6000
Wire Wire Line
	4100 6000 2650 6000
Wire Wire Line
	4100 5900 2650 5900
$Comp
L schematicProject-rescue:ABM8G-chip_media-cache-2018-09-26-20-57-17 X1
U 1 1 5BDFA591
P 2100 6000
F 0 "X1" H 2050 6150 60  0000 L CNN
F 1 "12MHz" H 1950 5850 60  0000 L CNN
F 2 "kicad-footprints:Crystal_SMD_Abracon_ABM8G-4Pin_3.2x2.5mm" H 2100 6000 60  0001 C CNN
F 3 "" H 2100 6000 60  0001 C CNN
	1    2100 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 6300 2100 6300
Wire Wire Line
	2650 6300 2650 6000
Wire Wire Line
	2650 5700 2100 5700
Wire Wire Line
	2650 5900 2650 5700
Wire Wire Line
	2100 6300 1750 6300
Connection ~ 2100 6300
Wire Wire Line
	2100 5700 1750 5700
Connection ~ 2100 5700
Wire Wire Line
	1700 6050 1700 6000
Wire Wire Line
	1700 6000 1400 6000
Connection ~ 1700 6000
Wire Wire Line
	1700 6000 1700 5950
Wire Wire Line
	1450 6300 1400 6300
Wire Wire Line
	1400 6300 1400 6000
Wire Wire Line
	1400 5700 1450 5700
Wire Wire Line
	1400 6000 1400 5700
$Comp
L schematicProject-rescue:GND-chip_media-cache-2018-09-25-05-03-38 #PWR0154
U 1 1 5C2D5D6B
P 19900 10950
F 0 "#PWR0154" H 19900 10700 50  0001 C CNN
F 1 "GND" H 19905 10777 50  0000 C CNN
F 2 "" H 19900 10950 50  0000 C CNN
F 3 "" H 19900 10950 50  0000 C CNN
	1    19900 10950
	1    0    0    1   
$EndComp
Wire Wire Line
	20450 10950 19900 10950
Wire Wire Line
	20950 11050 21400 11050
Wire Wire Line
	20450 11050 20000 11050
Wire Wire Line
	20450 11150 20000 11150
Wire Wire Line
	20450 11250 20000 11250
Wire Wire Line
	20950 11150 21400 11150
Wire Wire Line
	20950 11250 21400 11250
Wire Wire Line
	20450 11350 20000 11350
Wire Wire Line
	20950 11350 21400 11350
Text Label 21400 11050 2    50   ~ 0
ADC1
Text Label 20000 11050 0    50   ~ 0
ADC0
Text Label 20000 11150 0    50   ~ 0
GPIO0
Text Label 20000 11250 0    50   ~ 0
GPIO2
Text Label 21400 11150 2    50   ~ 0
GPIO1
Text Label 21400 11250 2    50   ~ 0
GPIO3
Text Label 20000 11350 0    50   ~ 0
GPIO4
Text Label 21400 11350 2    50   ~ 0
GPIO5
Text HLabel 14150 9900 2    60   Output ~ 0
FMC_CLK
Wire Wire Line
	17500 3500 18050 3500
Wire Wire Line
	17500 3600 18050 3600
Text GLabel 8800 3550 2    50   Input ~ 0
VDD_STM32
Wire Wire Line
	8800 3550 8700 3550
Text GLabel 6100 1350 0    50   Input ~ 0
VDD_STM32
Wire Wire Line
	6100 1350 6200 1350
Wire Wire Line
	6100 1550 6200 1550
Wire Wire Line
	6200 1350 6200 1550
Connection ~ 6200 1550
Wire Wire Line
	6200 1550 6900 1550
Text GLabel 5950 12000 2    50   Input ~ 0
VDD_STM32
Wire Wire Line
	5950 12000 5850 12000
Wire Wire Line
	5600 12200 5850 12200
Wire Wire Line
	5850 12000 5850 12200
Connection ~ 5850 12200
Wire Wire Line
	5850 12200 6050 12200
Text HLabel 21100 10950 2    50   Input ~ 0
STM3V3
Wire Wire Line
	21100 10950 20950 10950
Text GLabel 2650 2750 0    50   Input ~ 0
VDD_STM32
Wire Wire Line
	2650 2750 2800 2750
Text GLabel 3950 5100 0    50   Input ~ 0
VDD_STM32
Wire Wire Line
	3950 5100 4100 5100
Text GLabel 3950 4600 0    50   Input ~ 0
VDD_STM32
Wire Wire Line
	3950 4600 4100 4600
Wire Wire Line
	13500 10300 14150 10300
Wire Wire Line
	13500 10700 13850 10700
Wire Wire Line
	13500 10800 13850 10800
Wire Wire Line
	13500 10900 13850 10900
Wire Wire Line
	3700 9800 4100 9800
Wire Wire Line
	3700 9900 4100 9900
Wire Wire Line
	3700 10000 4100 10000
Wire Wire Line
	3700 10100 4100 10100
Wire Wire Line
	3700 10200 4100 10200
Entry Wire Line
	17250 12250 17350 12350
Entry Wire Line
	17250 12350 17350 12450
Entry Wire Line
	17250 12450 17350 12550
Entry Wire Line
	17250 11950 17350 12050
Entry Wire Line
	17250 12050 17350 12150
Entry Wire Line
	17250 12150 17350 12250
Entry Wire Line
	17250 11650 17350 11750
Entry Wire Line
	17250 11750 17350 11850
Entry Wire Line
	17250 11850 17350 11950
Entry Wire Line
	17250 12850 17350 12950
Entry Wire Line
	17250 12950 17350 13050
Entry Wire Line
	17250 13050 17350 13150
Entry Wire Line
	17250 12550 17350 12650
Entry Wire Line
	17250 12650 17350 12750
Entry Wire Line
	17250 12750 17350 12850
Wire Bus Line
	17100 11500 17250 11500
Wire Wire Line
	17350 11750 17700 11750
Wire Wire Line
	17350 11850 17700 11850
Wire Wire Line
	17350 11950 17700 11950
Wire Wire Line
	17350 12050 17700 12050
Wire Wire Line
	17350 12150 17700 12150
Wire Wire Line
	17350 12250 17700 12250
Wire Wire Line
	17350 12350 17700 12350
Wire Wire Line
	17350 12450 17700 12450
Wire Wire Line
	17350 12550 17700 12550
Wire Wire Line
	17350 12650 17700 12650
Wire Wire Line
	17350 12750 17700 12750
Wire Wire Line
	17350 12850 17700 12850
Wire Wire Line
	17350 12950 17700 12950
Wire Wire Line
	17350 13050 17700 13050
Wire Wire Line
	17350 13150 17700 13150
Entry Wire Line
	16400 12650 16500 12750
Entry Wire Line
	16400 12750 16500 12850
Entry Wire Line
	16400 12850 16500 12950
Entry Wire Line
	16400 12350 16500 12450
Entry Wire Line
	16400 12450 16500 12550
Entry Wire Line
	16400 12550 16500 12650
Entry Wire Line
	16400 12050 16500 12150
Entry Wire Line
	16400 12150 16500 12250
Entry Wire Line
	16400 12250 16500 12350
Wire Bus Line
	16250 11900 16400 11900
Wire Wire Line
	16500 12150 16850 12150
Wire Wire Line
	16500 12250 16850 12250
Wire Wire Line
	16500 12350 16850 12350
Wire Wire Line
	16500 12450 16850 12450
Wire Wire Line
	16500 12550 16850 12550
Wire Wire Line
	16500 12650 16850 12650
Wire Wire Line
	16500 12850 16850 12850
Wire Wire Line
	16500 12950 16850 12950
Text Label 17700 11750 2    50   ~ 0
FMC_D0
Text Label 17700 11850 2    50   ~ 0
FMC_D1
Text Label 17700 11950 2    50   ~ 0
FMC_D2
Text Label 17700 12050 2    50   ~ 0
FMC_D3
Text Label 17700 12150 2    50   ~ 0
FMC_D4
Text Label 17700 12250 2    50   ~ 0
FMC_D5
Text Label 17700 12350 2    50   ~ 0
FMC_D6
Text Label 17700 12450 2    50   ~ 0
FMC_D7
Text Label 17700 12550 2    50   ~ 0
FMC_D8
Text Label 17700 12650 2    50   ~ 0
FMC_D9
Text Label 17700 12750 2    50   ~ 0
FMC_D10
Text Label 17700 12850 2    50   ~ 0
FMC_D11
Text Label 17700 12950 2    50   ~ 0
FMC_D12
Text Label 17700 13050 2    50   ~ 0
FMC_D13
Text Label 17700 13150 2    50   ~ 0
FMC_D14
Entry Wire Line
	17250 13150 17350 13250
Wire Wire Line
	17350 13250 17700 13250
Text Label 17700 13250 2    50   ~ 0
FMC_D15
Text Label 16850 12150 2    50   ~ 0
FMC_A16
Text Label 16850 12250 2    50   ~ 0
FMC_A17
Text Label 16850 12350 2    50   ~ 0
FMC_A18
Text Label 16850 12450 2    50   ~ 0
FMC_A19
Text Label 16850 12550 2    50   ~ 0
FMC_A20
Text Label 16850 12650 2    50   ~ 0
FMC_A21
Text Label 16850 12750 2    50   ~ 0
FMC_A22
Text Label 16850 12850 2    50   ~ 0
FMC_A23
Text Label 16850 12950 2    50   ~ 0
FMC_A24
Text HLabel 17100 11500 0    50   BiDi ~ 0
FMC_D[15..0]
Text HLabel 16250 11900 0    50   Output ~ 0
FMC_A[24..16]
Wire Wire Line
	16850 12750 16500 12750
Text Label 3700 7500 0    50   ~ 0
FMC_A24
Wire Wire Line
	3700 7500 4100 7500
Wire Wire Line
	18050 3400 17450 3400
Text Label 17450 3400 0    50   ~ 0
SP_M0
Wire Wire Line
	18050 3300 17450 3300
Text Label 17450 3300 0    50   ~ 0
DIR_M0
NoConn ~ 13500 7400
NoConn ~ 13500 7500
Text Label 19700 5400 0    67   ~ 0
VM
Text Label 19800 7300 0    67   ~ 0
VM
NoConn ~ 18600 7350
NoConn ~ 18600 7250
NoConn ~ 17300 7350
NoConn ~ 17300 7250
Text Label 16500 7150 0    71   ~ 0
CM2D
Wire Wire Line
	17300 7150 16500 7150
$Comp
L schematicProject-rescue:CONN_01X05-chip_media-cache-2018-09-25-05-03-38 P2
U 1 1 5BD7B8C0
P 20150 7100
F 0 "P2" H 20228 7141 50  0000 L CNN
F 1 "CONN_01X05" H 20228 7050 50  0000 L CNN
F 2 "kicad footprints:PinHeader_1x05_P2.54mm_Vertical" H 20150 7100 50  0001 C CNN
F 3 "" H 20150 7100 50  0000 C CNN
	1    20150 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	19150 6900 19950 6900
Wire Wire Line
	19550 7000 19950 7000
Wire Wire Line
	19400 7100 19950 7100
Wire Wire Line
	19550 7200 19950 7200
Wire Wire Line
	19600 7300 19950 7300
Wire Wire Line
	19600 6750 19200 6750
Wire Wire Line
	19600 7300 19600 6750
Connection ~ 19600 7300
Wire Wire Line
	19550 7300 19600 7300
Wire Wire Line
	19550 7050 19550 7000
Wire Wire Line
	19550 7150 19550 7200
Wire Wire Line
	18600 6750 18900 6750
$Comp
L schematicProject-rescue:R-chip_media-cache-2018-09-25-05-03-38 R13
U 1 1 5BE087C8
P 19050 6750
F 0 "R13" V 18843 6750 50  0000 C CNN
F 1 "1k" V 18934 6750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 18980 6750 50  0001 C CNN
F 3 "" H 19050 6750 50  0000 C CNN
	1    19050 6750
	0    1    1    0   
$EndComp
Text Label 16500 7050 0    71   ~ 0
CM2B
Text Label 16500 6950 0    71   ~ 0
CM2A
Text Label 16500 6850 0    71   ~ 0
CM2C
Wire Wire Line
	17300 7050 16500 7050
Wire Wire Line
	17300 6950 16500 6950
Wire Wire Line
	17300 6850 16500 6850
Wire Wire Line
	19550 7550 19550 7300
Wire Wire Line
	19100 7550 19550 7550
Wire Wire Line
	18600 7550 18900 7550
$Comp
L schematicProject-rescue:D_Zener_Small-chip_media-cache-2018-09-25-05-03-38 D6
U 1 1 5BD7BB52
P 19000 7550
F 0 "D6" H 19000 7755 50  0000 C CNN
F 1 "D_Zener_Small" H 19000 7664 50  0000 C CNN
F 2 "kicad footprints:D_SOD-123F" V 19000 7550 50  0001 C CNN
F 3 "" V 19000 7550 50  0000 C CNN
	1    19000 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	18600 7150 19550 7150
Wire Wire Line
	18600 7050 19550 7050
Wire Wire Line
	19400 6850 19400 7100
Wire Wire Line
	18600 6850 19400 6850
Wire Wire Line
	19150 6950 19150 6900
Wire Wire Line
	18600 6950 19150 6950
$Comp
L schematicProject-rescue:ULN2003-chip_media-cache-2018-09-25-05-03-38 U10
U 1 1 5BB01C55
P 17950 7150
F 0 "U10" H 17950 7815 50  0000 C CNN
F 1 "ULN2003" H 17950 7724 50  0000 C CNN
F 2 "housinngs:TSSOP-16_4.4x5mm_Pitch0.65mm" H 17950 7723 50  0001 C CNN
F 3 "" H 17950 7150 50  0000 C CNN
	1    17950 7150
	1    0    0    -1  
$EndComp
NoConn ~ 4100 8100
Wire Wire Line
	13500 7700 14100 7700
Wire Wire Line
	13500 7600 14100 7600
$Comp
L schematicProject-rescue:STM32F446ZETx-MCU_ST_STM32 U8
U 1 1 5B7EAE11
P 8800 7800
F 0 "U8" H 9850 4050 50  0000 C CNN
F 1 "STM32F446ZETx" H 9850 3950 50  0000 C CNN
F 2 "Package_QFP:TQFP-144_20x20mm_P0.5mm" H 13300 11475 50  0001 R TNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00141306.pdf" H 8800 7800 50  0001 C CNN
	1    8800 7800
	1    0    0    -1  
$EndComp
NoConn ~ 4100 9000
Wire Wire Line
	13500 8700 14100 8700
Text Label 14100 8700 2    50   ~ 0
GPIO2
Wire Wire Line
	13500 8800 14100 8800
Text Label 14100 8800 2    50   ~ 0
GPIO3
NoConn ~ 13500 6700
NoConn ~ 13500 6800
NoConn ~ 13500 7000
NoConn ~ 13500 7100
NoConn ~ 13500 5700
NoConn ~ 4100 8400
NoConn ~ 4100 8500
NoConn ~ 4100 8900
NoConn ~ 4100 8200
NoConn ~ 4100 8300
NoConn ~ 4100 8600
$Comp
L Connector:Conn_02x06_Odd_Even J4
U 1 1 5C9AFE18
P 20650 11150
F 0 "J4" H 20700 11567 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 20700 11476 50  0000 C CNN
F 2 "kicad footprints:PinHeader_2x06_P2.54mm_Vertical" H 20650 11150 50  0001 C CNN
F 3 "~" H 20650 11150 50  0001 C CNN
	1    20650 11150
	1    0    0    -1  
$EndComp
Wire Wire Line
	20950 11450 21400 11450
Text Label 21400 11450 2    50   ~ 0
GPIO7
Wire Wire Line
	20450 11450 20000 11450
Text Label 20000 11450 0    50   ~ 0
GPIO6
Wire Wire Line
	14950 9000 15350 9000
Wire Wire Line
	14950 9600 15350 9600
Connection ~ 15350 9000
Wire Wire Line
	15350 9000 15850 9000
$Comp
L device:Crystal Y1
U 1 1 5CB2F0C2
P 15350 9300
F 0 "Y1" V 15396 9169 50  0000 R CNN
F 1 "32.768kHz" V 15305 9169 50  0000 R CNN
F 2 "kicad footprints:Crystal_SMD_3215-2Pin_3.2x1.5mm" H 15350 9300 50  0001 C CNN
F 3 "" H 15350 9300 50  0001 C CNN
	1    15350 9300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	15350 9000 15350 9150
Wire Wire Line
	15350 9450 15350 9600
Connection ~ 15350 9600
Wire Wire Line
	15350 9600 15850 9600
NoConn ~ 17300 6750
Wire Wire Line
	10750 12000 10750 11900
Text HLabel 10750 11900 1    50   Input ~ 0
STM3V3
Text GLabel 12000 12950 2    50   Input ~ 0
VDD_STM32
Wire Wire Line
	12000 12950 11900 12950
Wire Wire Line
	11900 12950 11900 12800
$Comp
L schematicProject-rescue:C-device C53
U 1 1 5BE0EEDD
P 17400 3050
F 0 "C53" H 17515 3096 50  0000 L CNN
F 1 "0.1uF" H 17515 3005 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 17438 2900 50  0001 C CNN
F 3 "" H 17400 3050 50  0001 C CNN
	1    17400 3050
	0    1    1    0   
$EndComp
$Comp
L schematicProject-rescue:C-device C54
U 1 1 5BE0EEE4
P 19950 3100
F 0 "C54" H 20065 3146 50  0000 L CNN
F 1 "4.7uF" H 20065 3055 50  0000 L CNN
F 2 "kicad-footprints:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 19988 2950 50  0001 C CNN
F 3 "" H 19950 3100 50  0001 C CNN
	1    19950 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	19350 3100 19800 3100
Wire Wire Line
	20300 3100 20100 3100
Wire Bus Line
	16400 11900 16400 12850
Wire Bus Line
	17250 11500 17250 13250
$EndSCHEMATC
